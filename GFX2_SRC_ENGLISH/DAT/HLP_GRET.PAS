{
  Ce fichier contient la partie "Greetings ..." de l'aide de GRAFX 2
}

procedure Generer_l_aide_Greetings;
begin
  Demarrer_section_d_aide;
                        {XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}
                        {XXXXXXXXXXXXXXXXXXXXXX}
  Ecrire_ligne_d_aide(T,'GREETINGS:');
  Ecrire_ligne_d_aide(N,'');
  Ecrire_ligne_d_aide(N,'Our best regards go to...');
  Ecrire_ligne_d_aide(N,'');
  Ecrire_ligne_d_aide(N,'  Access        Filter        Pink');
  Ecrire_ligne_d_aide(N,'  Ace           Fiver         Pixel');
  Ecrire_ligne_d_aide(N,'  AcidJam       Flan          Profil');
  Ecrire_ligne_d_aide(N,'  Acryl         Fred          Prowler');
  Ecrire_ligne_d_aide(N,'  Alexel        FreddyV       Puznik');
  Ecrire_ligne_d_aide(N,'  Alias         Frost         Quick');
  Ecrire_ligne_d_aide(N,'  Amiral        Ga�l(GDC)     Ra');
  Ecrire_ligne_d_aide(N,'  Arrakis       GainX         Raster');
  Ecrire_ligne_d_aide(N,'  Avocado       Gandalf       Ravian');
  Ecrire_ligne_d_aide(N,'  Baloo         Goblin        RedBug');
  Ecrire_ligne_d_aide(N,'  Barti         Greenpix7     Rem');
  Ecrire_ligne_d_aide(N,'  Bat           Grid          Rez');
  Ecrire_ligne_d_aide(N,'  Biro          GrosQuick     Roudoudou');
  Ecrire_ligne_d_aide(N,'  Bisounours    HackerCroll   Sacrilege');
  Ecrire_ligne_d_aide(N,'  BlackAxe      Haplo         Sam');
  Ecrire_ligne_d_aide(N,'  Bonnie        Hof           SandMan');
  Ecrire_ligne_d_aide(N,'  Boo           Hornet        Scape');
  Ecrire_ligne_d_aide(N,'  Boz           Hulud         S�bastien');
  Ecrire_ligne_d_aide(N,'  Carine        Java          Shodan');
  Ecrire_ligne_d_aide(N,'  Chandra       JBT           Skal');
  Ecrire_ligne_d_aide(N,'  Cheetah       J�r�me        Skyfire');
  Ecrire_ligne_d_aide(N,'  Chill         Julien(JCA)   Sphair');
  Ecrire_ligne_d_aide(N,'  Cougar        KalMinDo      Sprocket');
  Ecrire_ligne_d_aide(N,'  Cremax        KaneWood      Stef');
  Ecrire_ligne_d_aide(N,'  Cyclone       Karma         Stony');
  Ecrire_ligne_d_aide(N,'  Dake          Keith303      Sumaleth');
  Ecrire_ligne_d_aide(N,'  Danny         Lazur         Sunday');
  Ecrire_ligne_d_aide(N,'  Danube        LightShow     Suny');
  Ecrire_ligne_d_aide(N,'  Darjul        Lluvia        Sybaris');
  Ecrire_ligne_d_aide(N,'  Darwin        Louie         TBF');
  Ecrire_ligne_d_aide(N,'  DarkAngel     Luk           Tempest');
  Ecrire_ligne_d_aide(N,'  Das           Made          Thor');
  Ecrire_ligne_d_aide(N,'  Decker        Mamos         TMK');
  Ecrire_ligne_d_aide(N,'  DerPiipo      Mandrixx      TwoFace');
  Ecrire_ligne_d_aide(N,'  Destop        Mangue        Underking');
  Ecrire_ligne_d_aide(N,'  Diabolo       Mars          Unreal');
  Ecrire_ligne_d_aide(N,'  DineS         Mephisto      VaeVictis');
  Ecrire_ligne_d_aide(N,'  Drac          Mercure       Vastator');
  Ecrire_ligne_d_aide(N,'  DrYes         Mirec         Vatin');
  Ecrire_ligne_d_aide(N,'  Edyx          Moa           Veckman');
  Ecrire_ligne_d_aide(N,'  Eller         Moxica        Wain');
  Ecrire_ligne_d_aide(N,'  Ellyn         MRK           Wally');
  Ecrire_ligne_d_aide(N,'  EOF           Nitch         WillBe');
  Ecrire_ligne_d_aide(N,'  Fall          Noal          Xoomie');
  Ecrire_ligne_d_aide(N,'  Fame          Nytrik        Xtrm');
  Ecrire_ligne_d_aide(N,'  Fantom        Optic         YannSulu');
  Ecrire_ligne_d_aide(N,'  Fear          Orome         Z');
  Ecrire_ligne_d_aide(N,'  Feather       Pahladin      Zeb');
  Ecrire_ligne_d_aide(N,'  Fennec        Phar          Zebig');
  Ecrire_ligne_d_aide(N,'  and all #pixel, #demofr and #coders.');
                        {XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}
                        {XXXXXXXXXXXXXXXXXXXXXX}
  Terminer_section_d_aide;
end;
