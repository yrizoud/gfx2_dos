#include "const.h"
#include "struct.h"
#include "global.h"
#include "graph.h"
#include "divers.h"
#include "pages.h"
#include "op_c.h"
#include <fcntl.h>
#include <sys\stat.h>
#include <io.h>
#include <stdio.h>
#include <dos.h>
#include <mem.h>
#include <stdlib.h>

//  On d�clare m�chamment le prototype de Error pour �viter de faire un
// fichier "main.h":
void Error(int Code);


// Chargement des pixels dans l'�cran principal
void Pixel_load_in_current_screen(short Pos_X,short Pos_Y,byte color)
{
  if ((Pos_X>=0) && (Pos_Y>=0))
  if ((Pos_X<Main_image_width) && (Pos_Y<Main_image_height))
    Pixel_in_current_screen(Pos_X,Pos_Y,color);
}


// Chargement des pixels dans la brosse
void Pixel_load_in_brush(short Pos_X,short Pos_Y,byte color)
{
  if ((Pos_X>=0) && (Pos_Y>=0))
  if ((Pos_X<Brush_width) && (Pos_Y<Brush_height))
    Pixel_dans_Brosse(Pos_X,Pos_Y,color);
}


short Preview_factor_X;
short Preview_factor_Y;
short Preview_pos_X;
short Preview_pos_Y;


// Chargement des pixels dans la preview
void Pixel_load_in_preview(short Pos_X,short Pos_Y,byte color)
{
  if (((Pos_X % Preview_factor_X)==0) && ((Pos_Y % Preview_factor_Y)==0))
  if ((Pos_X<Main_image_width) && (Pos_Y<Main_image_height))
    Pixel(Preview_pos_X+(Pos_X/Preview_factor_X),
          Preview_pos_Y+(Pos_Y/Preview_factor_Y),
          color);
}


void Remap_fileselector(void)
{
  if (Pixel_load_function==Pixel_load_in_preview)
  {
    Compute_optimal_menu_colors(Main_palette);
    Remap_screen_after_menu_colors_change();
  }
}



// Donn�es pour la gestion du chargement en 24b
#define FORMAT_24B 0x100
typedef void (* Func_24b_display) (short,short,byte,byte,byte);
int                    Image_24b;
struct T_Components *   Buffer_image_24b;
Func_24b_display Pixel_load_24b;


// Chargement des pixels dans le buffer 24b
void Pixel_load_in_24b_buffer(short Pos_X,short Pos_Y,byte R,byte V,byte B)
{
  int indice;

  if ((Pos_X>=0) && (Pos_Y>=0))
  if ((Pos_X<Main_image_width) && (Pos_Y<Main_image_height))
  {
    indice=(Pos_Y*Main_image_width)+Pos_X;
    Buffer_image_24b[indice].R=R;
    Buffer_image_24b[indice].V=V;
    Buffer_image_24b[indice].B=B;
  }
}

// Chargement des pixels dans la preview en 24b
void Pixel_load_in_24b_preview(short Pos_X,short Pos_Y,byte R,byte V,byte B)
{
  byte color;

  if (((Pos_X % Preview_factor_X)==0) && ((Pos_Y % Preview_factor_Y)==0))
  if ((Pos_X<Main_image_width) && (Pos_Y<Main_image_height))
  {
    color=((R >> 5) << 5) |
            ((V >> 5) << 2) |
            ((B >> 6));
    Pixel(Preview_pos_X+(Pos_X/Preview_factor_X),
          Preview_pos_Y+(Pos_Y/Preview_factor_Y),
          color);
  }
}

// Cr�ation d'une palette fake
void Set_palette_fake_24b(T_Palette Palette)
{
  int color;

  // G�n�ration de la palette
  for (color=0;color<256;color++)
  {
    Palette[color].R=((color & 0xE0)>>5)<<3;
    Palette[color].V=((color & 0x1C)>>2)<<3;
    Palette[color].B=((color & 0x03)>>0)<<4;
  }
}

// Suppl�ment � faire lors de l'initialisation d'une preview dans le cas
// d'une image 24b
void Init_preview_24b(int Largeur,int Hauteur)
{
  if (Pixel_load_function==Pixel_load_in_preview)
  {
    // Aiguillage du chargement 24b
    Pixel_load_24b=Pixel_load_in_24b_preview;

    // Changement de palette
    Set_palette_fake_24b(Main_palette);
    Set_palette(Main_palette);
    Remap_fileselector();
  }
  else
  {
    // Aiguillage du chargement 24b
    Pixel_load_24b=Pixel_load_in_24b_buffer;

    // Allocation du buffer 24b
    Buffer_image_24b=
      (struct T_Components *)Borrow_memory_from_page(Largeur*Hauteur*sizeof(struct T_Components));
    if (!Buffer_image_24b)
    {
      // Afficher un message d'erreur

      // Pour �tre s�r que ce soit lisible.
      Compute_optimal_menu_colors(Main_palette);
      Message_out_of_memory();
      if (Pixel_load_function==Pixel_load_in_current_screen)
        File_error=1; // 1 => On n'a pas perdu l'image courante
      else
        File_error=3; // 3 => Chargement de brosse �chou�
    }
    else
      Image_24b=1;        // On a un buffer � traiter en fin de chargement
  }
}




void Init_preview(short Largeur,short Hauteur,long Taille,int Format)
//
//   Cette proc�dure doit �tre appel�e par les routines de chargement
// d'images.
//   Elle doit �tre appel�e entre le moment o� l'on connait la dimension de
// l'image (dimension r�elle, pas dimension tronqu�e) et l'affichage du
// premier point.
//
{
  char  str[256];
  int   image_is_24b;

  image_is_24b=Format & FORMAT_24B;
  Format      =Format & (~FORMAT_24B);

  if (Pixel_load_function==Pixel_load_in_preview)
  {
    // Pr�paration du chargement d'une preview:

    // Affichage des donn�es "Image size:"
    if ((Largeur<10000) && (Hauteur<10000))
    {
      Num2str(Largeur,str,4);
      Num2str(Hauteur,str+5,4);
      str[4]='x';
      Print_in_window(226,55,str,MC_Black,MC_Light);
    }
    else
    {
      Print_in_window(226,55,"VERY BIG!",MC_Black,MC_Light);
    }

    // Affichage de la taille du fichier
    if (Taille<1048576)
    {
      // Le fichier fait moins d'un Mega, on affiche sa taille direct
      Num2str(Taille,str,9);
      Print_in_window(226,63,str,MC_Black,MC_Light);
    }
    else if ((Taille/1024)<10000000)
    {
      // Le fichier fait plus d'un Mega, on peut afficher sa taille en Ko
      Num2str(Taille/1024,str,7);
      str[7]='K';
      str[8]='b';
      Print_in_window(226,63,str,MC_Black,MC_Light);
    }
    else
    {
      // Le fichier fait plus de 10 Giga octets (cas tr�s rare :))
      Print_in_window(226,63,"TOO BIG!!",MC_Black,MC_Light);
    }

    // Affichage du vrai format
    if (Format!=Main_format)
    {
      Print_in_window( 27,74,"but is:",MC_Dark,MC_Light);
      Print_in_window( 83,74,Format_Extension[Format-1],MC_Black,MC_Light);
    }

    // On efface le commentaire pr�c�dent
    Block(Window_pos_X+46*Menu_factor_X,Window_pos_Y+176*Menu_factor_Y,
          Menu_factor_X<<8,Menu_factor_Y<<3,MC_Light);
    // Affichage du commentaire
    if (Format_Commentaire[Format-1])
      Print_in_window(46,176,Main_comment,MC_Black,MC_Light);

    // Calculs des donn�es n�cessaires � l'affichage de la preview:
    Preview_factor_X=Round_div_max(Largeur,122*Menu_factor_X);
    Preview_factor_Y=Round_div_max(Hauteur, 82*Menu_factor_Y);

    if ( (!Config.Maximize_preview) && (Preview_factor_X!=Preview_factor_Y) )
    {
      if (Preview_factor_X>Preview_factor_Y)
        Preview_factor_Y=Preview_factor_X;
      else
        Preview_factor_X=Preview_factor_Y;
    }

    Preview_pos_X=Window_pos_X+180*Menu_factor_X;
    Preview_pos_Y=Window_pos_Y+ 89*Menu_factor_Y;

    // On nettoie la zone o� va s'afficher la preview:
    Block(Preview_pos_X,Preview_pos_Y,
          Round_div_max(Largeur,Preview_factor_X),
          Round_div_max(Hauteur,Preview_factor_Y),
          MC_Black);
  }
  else
  {
    if (Pixel_load_function==Pixel_load_in_current_screen)
    {
      if (Backup_with_new_dimensions(0,Largeur,Hauteur))
      {
        // La nouvelle page a pu �tre allou�e, elle est pour l'instant pleine
        // de 0s. Elle fait Main_image_width de large.
        // Normalement tout va bien, tout est sous contr�le...
      }
      else
      {
        // Afficher un message d'erreur

        // Pour �tre s�r que ce soit lisible.
        Compute_optimal_menu_colors(Main_palette);
        Message_out_of_memory();
        File_error=1; // 1 => On n'a pas perdu l'image courante
      }
    }
    else // chargement dans la brosse
    {
      free(Brush);
      free(Smear_brush);
      Brush=(byte *)malloc(Largeur*Hauteur);
      Brush_width=Largeur;
      Brush_height=Hauteur;
      if (Brush)
      {
        Smear_brush=(byte *)malloc(Largeur*Hauteur);
        if (!Smear_brush)
          File_error=3;
      }
      else
        File_error=3;
    }
  }

  if (!File_error)
    if (image_is_24b)
      Init_preview_24b(Largeur,Hauteur);
}



void Draw_palette_preview(void)
{
  short index;
  short Preview_pos_X=Window_pos_X+186*Menu_factor_X;
  short Preview_pos_Y=Window_pos_Y+ 90*Menu_factor_Y;

  if (Pixel_load_function==Pixel_load_in_preview)
    for (index=0; index<256; index++)
      Block(Preview_pos_X+(((index>>4)*7)*Menu_factor_X),
            Preview_pos_Y+(((index&15)*5)*Menu_factor_Y),
            5*Menu_factor_X,5*Menu_factor_Y,index);
}



// Calcul du nom complet du fichier
void filename_complet(char * filename, byte is_colorix_format)
{
  byte Pos;

  strcpy(filename,Main_file_directory);

  if (filename[strlen(filename)-1]!='\\')
    strcat(filename,"\\");

  // Si on est en train de sauvegarder une image Colorix, on calcule son ext.
  if (is_colorix_format)
  {
    Pos=strlen(Main_filename)-1;
    if (Main_filename[Pos]=='?')
    {
      if (Main_image_width<=320)
        Main_filename[Pos]='I';
      else
      {
        if (Main_image_width<=360)
          Main_filename[Pos]='Q';
        else
        {
          if (Main_image_width<=640)
            Main_filename[Pos]='F';
          else
          {
            if (Main_image_width<=800)
              Main_filename[Pos]='N';
            else
              Main_filename[Pos]='O';
          }
        }
      }
    }
  }

  strcat(filename,Main_filename);
}


/////////////////////////////////////////////////////////////////////////////
//                    Gestion des lectures et �critures                    //
/////////////////////////////////////////////////////////////////////////////

byte * Tampon_lecture;
word   Index_lecture;

void Init_lecture(void)
{
  Tampon_lecture=(byte *)malloc(64000);
  Index_lecture=64000;
}

byte Read_one_byte(int Fichier)
{
  if (++Index_lecture>=64000)
  {
    if (read(Fichier,Tampon_lecture,64000)<=0)
      File_error=2;
    Index_lecture=0;
  }
  return Tampon_lecture[Index_lecture];
}

void Close_lecture(void)
{
  free(Tampon_lecture);
}

// --------------------------------------------------------------------------

byte * Write_buffer;
word   Write_buffer_index;

void Init_write_buffer(void)
{
  Write_buffer=(byte *)malloc(64000);
  Write_buffer_index=0;
}

void Write_one_byte(int Fichier, byte Octet)
{
  Write_buffer[Write_buffer_index++]=Octet;
  if (Write_buffer_index>=64000)
  {
    if (write(Fichier,Write_buffer,64000)==-1)
      File_error=1;
    Write_buffer_index=0;
  }
}

void End_write(int Fichier)
{
  if (Write_buffer_index)
    if (write(Fichier,Write_buffer,Write_buffer_index)==-1)
      File_error=1;
  free(Write_buffer);
}



/////////////////////////////////////////////////////////////////////////////

void (__interrupt __near *Ancien_handler_clavier)(void);
void __interrupt __near Nouveau_handler_clavier(void)
{
  _disable();
  if (!File_error)
    File_error=-1;
  _enable();
  _chain_intr(Ancien_handler_clavier);
}


// -------- Modifier la valeur du code d'erreur d'acc�s � un fichier --------
//   On n'est pas oblig� d'utiliser cette fonction � chaque fois mais il est
// important de l'utiliser dans les cas du type:
//   if (!File_error) *** else File_error=***;
// En fait, dans le cas o� l'on modifie File_error alors qu'elle contient
// d�j� un code d'erreur.
void Set_file_error(int value)
{
  if (File_error>=0)
    File_error=value;
}


// -- Charger n'importe connu quel type de fichier d'image (ou palette) -----
void Load_image(byte Image)
{
  int  index; // index de balayage des formats
  int  Format; // Format du fichier � charger


  // On place par d�faut File_error � vrai au cas o� on ne sache pas
  // charger le format du fichier:
  File_error=1;

  if (Main_format!=0)
  {
    Format_Test[Main_format-1]();
    if (!File_error)
      // Si dans le s�lecteur il y a un format valide on le prend tout de suite
      Format=Main_format-1;
  }

  if (File_error)
  {
    //  Sinon, on va devoir scanner les diff�rents formats qu'on connait pour
    // savoir � quel format est le fichier:
    for (index=0;index<NB_FORMATS_LOAD;index++)
    {
      // On appelle le testeur du format:
      Format_Test[index]();
      // On s'arr�te si le fichier est au bon format:
      if (File_error==0)
      {
        Format=index;
        break;
      }
    }
  }

  // Si on a su d�terminer avec succ�s le format du fichier:
  if (!File_error)
  {
    // Installer le handler d'interruption du clavier pour stopper une preview
    if (Pixel_load_function==Pixel_load_in_preview)
    {
      Ancien_handler_clavier=_dos_getvect(9);
      _dos_setvect(9,Nouveau_handler_clavier);
    }

    // On peut charger le fichier:
    Image_24b=0;
    Format_Load[Format]();

    // D�sinstaller le handler d'interruption du clavier
    if (Pixel_load_function==Pixel_load_in_preview)
      _dos_setvect(9,Ancien_handler_clavier);

    if (File_error>0)
      Error(0);

    if (Image_24b)
    {
      // On vient de charger une image 24b
      if (!File_error)
      {
        // Le chargement a r�ussi, on peut faire la conversion en 256 couleurs
        if (Pixel_load_function==Pixel_load_in_current_screen)
        {
          // Cas d'un chargement dans l'image
          if (Convert_24b_bitmap_to_256(Main_screen,Buffer_image_24b,Main_image_width,Main_image_height,Main_palette))
            File_error=2;
        }
        else
        {
          // Cas d'un chargement dans la brosse
          if (Convert_24b_bitmap_to_256(Brush,Buffer_image_24b,Brush_width,Brush_height,Main_palette))
            File_error=2;
        }
        if (!File_error)
          Palette_256_to_64(Main_palette);
      }

      free(Buffer_image_24b);
    }

    if (Image)
    {
      if ( (File_error!=1) && (Format_Backup_done[Format]) )
      {
        // On consid�re que l'image charg�e n'est plus modifi�e
        Main_image_is_modified=0;
        // Et on documente la variable Main_fileformat avec la valeur:
        Main_fileformat=Format+1;

        // Correction des dimensions
        if (Main_image_width<1)
          Main_image_width=1;
        if (Main_image_height<1)
          Main_image_height=1;
      }
      else if (File_error!=1)
      {
        // On consid�re que l'image charg�e est encore modifi�e
        Main_image_is_modified=1;
        // Et on documente la variable Main_fileformat avec la valeur:
        Main_fileformat=Format+1;
      }
      else
      {
        // Dans ce cas, on sait que l'image n'a pas chang�, mais ses
        // param�tres (dimension, palette, ...) si. Donc on les restaures.
        Download_infos_page_main(Main_backups->Pages);
      }
    }
  }
  else
    // Sinon, l'appelant sera au courant de l'�chec gr�ce � File_error;
    // et si on s'appr�tait � faire un chargement d�finitif de l'image (pas
    // une preview), alors on flash l'utilisateur.
    if (Pixel_load_function!=Pixel_load_in_preview)
      Error(0);
}


// -- Sauver n'importe quel type connu de fichier d'image (ou palette) ------
void Save_image(byte Image)
{
  // On place par d�faut File_error � vrai au cas o� on ne sache pas
  // sauver le format du fichier: (Est-ce vraiment utile??? Je ne crois pas!)
  File_error=1;

  Read_pixel_old=(Image)?Read_pixel_from_current_screen:Read_pixel_from_brush;

  Format_Save[Main_fileformat-1]();

  if (File_error)
    Error(0);
  else
  {
    if ((Image) && (Format_Backup_done[Main_fileformat-1]))
      Main_image_is_modified=0;
  }
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PAL ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// -- Tester si un fichier est au format PAL --------------------------------
void Test_PAL(void)
{
  int  Fichier;             // Handle du fichier
  char filename[256]; // Nom complet du fichier
  long file_size;   // Taille du fichier

  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Fichier=open(filename,O_RDONLY|O_BINARY);
  if (Fichier!=-1)
  {
    // Lecture de la taille du fichier
    file_size=filelength(Fichier);
    close(Fichier);
    // Le fichier ne peut �tre au format PAL que si sa taille vaut 768 octets
    if (file_size==sizeof(T_Palette))
      File_error=0;
  }
}


// -- Lire un fichier au format PAL -----------------------------------------
void Load_PAL(void)
{
  int   Handle;              // Handle du fichier
  char  filename[256]; // Nom complet du fichier
  long  file_size;   // Taille du fichier


  filename_complet(filename,0);
  File_error=0;

  // Ouverture du fichier
  Handle=open(filename,O_RDONLY|O_BINARY);
  if (Handle!=-1)
  {
    // Init_preview(???); // Pas possible... pas d'image...

    // Lecture du fichier dans Main_palette
    if (read(Handle,Main_palette,sizeof(T_Palette))==sizeof(T_Palette))
    {
      Set_palette(Main_palette);
      Remap_fileselector();

      // On dessine une preview de la palette (si chargement=preview)
      Draw_palette_preview();
    }
    else
      File_error=2;

    // Fermeture du fichier
    close(Handle);
  }
  else
    // Si on n'a pas r�ussi � ouvrir le fichier, alors il y a eu une erreur
    File_error=1;
}


// -- Sauver un fichier au format PAL ---------------------------------------
void Save_PAL(void)
{
  int  Fichier;             // Handle du fichier
  char filename[256]; // Nom complet du fichier
  long file_size;   // Taille du fichier

  filename_complet(filename,0);

  File_error=0;

  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    // Enregistrement de Main_palette dans le fichier
    if (write(Fichier,Main_palette,sizeof(T_Palette))==-1)
    {
      File_error=1;
      close(Fichier);
      remove(filename);
    }
    else // Ecriture correcte => Fermeture normale du fichier
      close(Fichier);
  }
  else // Si on n'a pas r�ussi � ouvrir le fichier, alors il y a eu une erreur
  {
    File_error=1;
    close(Fichier);
    remove(filename);
                     //   On se fout du r�sultat de l'op�ration car si �a
		     // renvoie 0 c'est que le fichier avait �t� partiel-
		     // -lement �crit, sinon pas du tout. Or dans tous les
		     // cas �a revient au m�me pour nous: Sauvegarde rat�e!
  }
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// IMG ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format IMG --------------------------------
void Test_IMG(void)
{
  int  Handle;              // Handle du fichier
  char filename[256]; // Nom complet du fichier
  struct Header
  {
    byte Filler1[6];
    word Largeur;
    word Hauteur;
    byte Filler2[118];
    T_Palette Palette;
  } IMG_Header;
  byte Signature[6]={0x01,0x00,0x47,0x12,0x6D,0xB0};


  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Handle=open(filename,O_RDONLY|O_BINARY);
  if (Handle!=-1)
  {
    // Lecture et v�rification de la signature
    if ((read(Handle,&IMG_Header,sizeof(struct Header)))==sizeof(struct Header))
    {
      if ( (!memcmp(IMG_Header.Filler1,Signature,6))
        && IMG_Header.Largeur && IMG_Header.Hauteur)
        File_error=0;
    }
    // Fermeture du fichier
    close(Handle);
  }
}


// -- Lire un fichier au format IMG -----------------------------------------
void Load_IMG(void)
{
  char filename[256]; // Nom complet du fichier
  byte * buffer;
  int  Fichier;
  word Pos_X,Pos_Y;
  long width_read;
  long file_size;
  struct Header
  {
    byte Filler1[6];
    word Largeur;
    word Hauteur;
    byte Filler2[118];
    T_Palette Palette;
  } IMG_Header;


  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);

    if (read(Fichier,&IMG_Header,sizeof(struct Header))==sizeof(struct Header))
    {
      buffer=(byte *)malloc(IMG_Header.Largeur);

      Init_preview(IMG_Header.Largeur,IMG_Header.Hauteur,file_size,FORMAT_IMG);
      if (File_error==0)
      {
        //   On commence par passer la palette en 256 comme �a, si la nouvelle
        // palette a moins de 256 coul, la pr�c�dente ne souffrira pas d'un
        // assombrissement pr�judiciable.
        Palette_64_to_256(Main_palette);
        //   On peut maintenant transf�rer la nouvelle palette
        memcpy(Main_palette,IMG_Header.Palette,sizeof(T_Palette));
        Palette_256_to_64(Main_palette);
        Set_palette(Main_palette);
        Remap_fileselector();

        Main_image_width=IMG_Header.Largeur;
        Main_image_height=IMG_Header.Hauteur;

        for (Pos_Y=0;(Pos_Y<Main_image_height) && (!File_error);Pos_Y++)
        {
          if ((width_read=read(Fichier,buffer,Main_image_width))==Main_image_width)
            for (Pos_X=0; Pos_X<width_read;Pos_X++)
              Pixel_load_function(Pos_X,Pos_Y,buffer[Pos_X]);
          else
            File_error=2;
        }
      }

      free(buffer);
    }
    else
      File_error=1;

    close(Fichier);
  }
  else
    File_error=1;
}

// -- Sauver un fichier au format IMG ---------------------------------------
void Save_IMG(void)
{
  char  filename[256]; // Nom complet du fichier
  int   Fichier;
  short Pos_X,Pos_Y;
  struct Header
  {
    byte Filler1[6]; // Signature (??!)
    word Largeur;
    word Hauteur;
    byte Filler2[118];
    T_Palette Palette;
  } IMG_Header;
  byte Signature[6]={0x01,0x00,0x47,0x12,0x6D,0xB0};

  filename_complet(filename,0);

  File_error=0;

  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    memcpy(IMG_Header.Filler1,Signature,6);

    IMG_Header.Largeur=Main_image_width;
    IMG_Header.Hauteur=Main_image_height;

    memset(IMG_Header.Filler2,0,118);
    IMG_Header.Filler2[4]=0xFF;
    IMG_Header.Filler2[22]=64; // Lo(Longueur de la signature)
    IMG_Header.Filler2[23]=0;  // Hi(Longueur de la signature)
    memcpy(IMG_Header.Filler2+23,"GRAFX2 by SunsetDesign (IMG format taken from PV (c)W.Wiedmann)",64);

    Palette_64_to_256(Main_palette);
    memcpy(IMG_Header.Palette,Main_palette,sizeof(T_Palette));
    Palette_256_to_64(Main_palette);

    if (write(Fichier,&IMG_Header,sizeof(struct Header))!=-1)
    {
      Init_write_buffer();

      for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
        for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
          Write_one_byte(Fichier,Read_pixel_old(Pos_X,Pos_Y));

      End_write(Fichier);
      close(Fichier);

      if (File_error)
        remove(filename);
    }
    else // Error d'�criture (disque plein ou prot�g�)
    {
      close(Fichier);
      remove(filename);
      File_error=1;
    }
  }
  else
  {
    close(Fichier);
    remove(filename);
    File_error=1;
  }
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PKM ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format PKM --------------------------------
void Test_PKM(void)
{
  int  Fichier;             // Handle du fichier
  char filename[256]; // Nom complet du fichier
  struct Header
  {
    char Ident[3]; // Cha�ne "PKM" }
    byte Methode;  // M�thode de compression:
		   //   0 = compression en ligne (c)KM
		   //   autres = inconnues pour le moment
    byte recog1;   // Octet de reconnaissance sur 1 octet  }
    byte recog2;   // Octet de reconnaissance sur 2 octets }
    word Largeur;  // Largeur de l'image
    word Hauteur;  // Hauteur de l'image
    T_Palette Palette; // Palette RVB 256*3
    word Jump;     // Taille du saut entre le header et l'image:
		   //   On va s'en servir pour rajouter un commentaire
  } Head;


  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Fichier=open(filename,O_RDONLY|O_BINARY);
  if (Fichier!=-1)
  {
    // Lecture du header du fichier
    if (read(Fichier,&Head,sizeof(struct Header))==sizeof(struct Header))
    {
      // On regarde s'il y a la signature PKM suivie de la m�thode 0.
      // La constante "PKM" �tant un cha�ne, elle se termine toujours par 0.
      // Donc pas la peine de s'emm...er � regarder si la m�thode est � 0.
      if ( (!memcmp(&Head,"PKM",4)) && Head.Largeur && Head.Hauteur)
        File_error=0;
    }
    close(Fichier);
  }
}


// -- Lire un fichier au format PKM -----------------------------------------
void Load_PKM(void)
{
  int  Fichier;             // Handle du fichier
  char filename[256]; // Nom complet du fichier
  struct Header
  {
    char Ident[3]; // Cha�ne "PKM" }
    byte Methode;  // M�thode de compression:
                   //   0 = compression en ligne (c)KM
                   //   autres = inconnues pour le moment
    byte recog1;   // Octet de reconnaissance sur 1 octet  }
    byte recog2;   // Octet de reconnaissance sur 2 octets }
    word Largeur;  // Largeur de l'image
    word Hauteur;  // Hauteur de l'image
    T_Palette Palette; // Palette RVB 256*3
    word Jump;     // Taille du saut entre le header et l'image:
                   //   On va s'en servir pour rajouter un commentaire
  } Head;
  byte  color;
  byte  Octet;
  word  Mot;
  word  index;
  dword Compteur_de_pixels;
  dword Compteur_de_donnees_packees;
  dword image_size;
  dword Taille_pack;
  long  file_size;


  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);

    if (read(Fichier,&Head,sizeof(struct Header))==sizeof(struct Header))
    {
      Main_comment[0]='\0'; // On efface le commentaire
      if (Head.Jump)
      {
        index=0;
        while ( (index<Head.Jump) && (!File_error) )
        {
          if (read(Fichier,&Octet,1)==1)
          {
            index+=2; // On rajoute le "Field-id" et "le Field-size" pas encore lu
            switch (Octet)
            {
              case 0 : // Comment
                if (read(Fichier,&Octet,1)==1)
                {
                  if (Octet>COMMENT_SIZE)
                  {
                    color=Octet;              // On se sert de color comme
                    Octet=COMMENT_SIZE;   // variable temporaire
                    color-=COMMENT_SIZE;
                  }
                  else
                    color=0;

                  if (read(Fichier,Main_comment,Octet)==Octet)
                  {
                    index+=Octet;
                    Main_comment[Octet]='\0';
                    if (color)
                      if (lseek(Fichier,color,SEEK_CUR)==-1)
                        File_error=2;
                  }
                  else
                    File_error=2;
                }
                else
                  File_error=2;
                break;

              case 1 : // Dimensions de l'�cran d'origine
                if (read(Fichier,&Octet,1)==1)
                {
                  if (Octet==4)
                  {
                    index+=4;
                    if ( (read(Fichier,&Original_screen_X,2)!=2)
                      || (read(Fichier,&Original_screen_Y,2)!=2) )
                      File_error=2;
                  }
                  else
                    File_error=2;
                }
                else
                  File_error=2;
                break;

              case 2 : // color de transparence
                if (read(Fichier,&Octet,1)==1)
                {
                  if (Octet==1)
                  {
                    index++;
                    if (read(Fichier,&Back_color,1)!=1)
                      File_error=2;
                  }
                  else
                    File_error=2;
                }
                else
                  File_error=2;
                break;

              default:
                if (read(Fichier,&Octet,1)==1)
                {
                  index+=Octet;
                  if (lseek(Fichier,Octet,SEEK_CUR)==-1)
                    File_error=2;
                }
                else
                  File_error=2;
            }
          }
          else
            File_error=2;
        }
        if ( (!File_error) && (index!=Head.Jump) )
          File_error=2;
      }

      Init_lecture();

      if (!File_error)
      {
        Init_preview(Head.Largeur,Head.Hauteur,file_size,FORMAT_PKM);
        if (File_error==0)
        {
          Main_image_width=Head.Largeur;
          Main_image_height=Head.Hauteur;
          image_size=(dword)(Main_image_width*Main_image_height);

          memcpy(Main_palette,Head.Palette,sizeof(T_Palette));
          Set_palette(Main_palette);
          Remap_fileselector();

          Compteur_de_donnees_packees=0;
          Compteur_de_pixels=0;
          Taille_pack=filelength(Fichier)-sizeof(struct Header)-Head.Jump;

          // Boucle de d�compression:
          while ( (Compteur_de_pixels<image_size) && (Compteur_de_donnees_packees<Taille_pack) && (!File_error) )
          {
            Octet=Read_one_byte(Fichier);

            // Si ce n'est pas un octet de reconnaissance, c'est un pixel brut
            if ( (Octet!=Head.recog1) && (Octet!=Head.recog2) )
            {
              Pixel_load_function(Compteur_de_pixels % Main_image_width,
                                  Compteur_de_pixels / Main_image_width,
                                  Octet);
              Compteur_de_donnees_packees++;
              Compteur_de_pixels++;
            }
            else // Sinon, On regarde si on va d�compacter un...
            { // ... nombre de pixels tenant sur un byte
              if (Octet==Head.recog1)
              {
                color=Read_one_byte(Fichier);
                Octet=Read_one_byte(Fichier);
                for (index=0; index<Octet; index++)
                  Pixel_load_function((Compteur_de_pixels+index) % Main_image_width,
                                      (Compteur_de_pixels+index) / Main_image_width,
                                      color);
                Compteur_de_pixels+=Octet;
                Compteur_de_donnees_packees+=3;
              }
              else // ... nombre de pixels tenant sur un word
              {
                color=Read_one_byte(Fichier);
                Mot=(word)(Read_one_byte(Fichier)<<8)+Read_one_byte(Fichier);
                for (index=0; index<Mot; index++)
                  Pixel_load_function((Compteur_de_pixels+index) % Main_image_width,
                                      (Compteur_de_pixels+index) / Main_image_width,
                                      color);
                Compteur_de_pixels+=Mot;
                Compteur_de_donnees_packees+=4;
              }
            }
          }
        }
      }
      Close_lecture();
    }
    else // Lecture header impossible: Error ne modifiant pas l'image
      File_error=1;

    close(Fichier);
  }
  else // Ouv. fichier impossible: Error ne modifiant pas l'image
    File_error=1;
}


// -- Sauver un fichier au format PKM ---------------------------------------

  // Trouver quels sont les octets de reconnaissance
  void Find_recog(byte * recog1, byte * recog2)
  {
    dword Find_recon[256]; // Table d'utilisation de couleurs
    byte  best;   // Meilleure couleur pour recon (recon1 puis recon2)
    dword NBest;  // Nombre d'occurences de cette couleur
    word  index;


    // On commence par compter l'utilisation de chaque couleurs
    Count_used_colors(Find_recon);

    // Ensuite recog1 devient celle la moins utilis�e de celles-ci
    *recog1=0;
    best=1;
    NBest=1000000; // Une m�me couleur ne pourra jamais �tre utilis�e 1M de fois.
    for (index=1;index<=255;index++)
      if (Find_recon[index]<NBest)
      {
	best=index;
	NBest=Find_recon[index];
      }
    *recog1=best;

    // Enfin recog2 devient la 2�me moins utilis�e
    *recog2=0;
    best=0;
    NBest=1000000;
    for (index=0;index<=255;index++)
      if ( (Find_recon[index]<NBest) && (index!=*recog1) )
      {
	best=index;
	NBest=Find_recon[index];
      }
    *recog2=best;
  }


void save_PKM(void)
{
  char filename[256];
  int  Fichier;
  struct Header
  {
    char Ident[3]; // Cha�ne "PKM" }
    byte Methode;  // M�thode de compression:
		   //   0 = compression en ligne (c)KM
		   //   autres = inconnues pour le moment
    byte recog1;   // Octet de reconnaissance sur 1 octet  }
    byte recog2;   // Octet de reconnaissance sur 2 octets }
    word Largeur;  // Largeur de l'image
    word Hauteur;  // Hauteur de l'image
    T_Palette Palette; // Palette RVB 256*3
    word Jump;     // Taille du saut entre le header et l'image:
		   //   On va s'en servir pour rajouter un commentaire
  } Head;
  dword Compteur_de_pixels;
  dword image_size;
  word  repetitions;
  byte  last_color;
  byte  pixel_value;
  byte  comment_size;



  // Construction du header
  memcpy(Head.Ident,"PKM",3);
  Head.Methode=0;
  Find_recog(&Head.recog1,&Head.recog2);
  Head.Largeur=Main_image_width;
  Head.Hauteur=Main_image_height;
  memcpy(Head.Palette,Main_palette,sizeof(T_Palette));

  // Calcul de la taille du Post-Header
  Head.Jump=9; // 6 pour les dimensions de l'ecran + 3 pour la back-color
  comment_size=strlen(Main_comment);
  if (comment_size)
    Head.Jump+=comment_size+2;


  filename_complet(filename,0);

  File_error=0;

  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    // Ecriture du header
    if (write(Fichier,&Head,sizeof(struct Header))!=-1)
    {
      Init_write_buffer();

      // Ecriture du commentaire
      // (Compteur_de_pixels est utilis� ici comme simple indice de comptage)
      if (comment_size)
      {
        Write_one_byte(Fichier,0);
        Write_one_byte(Fichier,comment_size);
        for (Compteur_de_pixels=0; Compteur_de_pixels<comment_size; Compteur_de_pixels++)
          Write_one_byte(Fichier,Main_comment[Compteur_de_pixels]);
      }
      // Ecriture des dimensions de l'�cran
      Write_one_byte(Fichier,1);
      Write_one_byte(Fichier,4);
      Write_one_byte(Fichier,Screen_width&0xFF);
      Write_one_byte(Fichier,Screen_width>>8);
      Write_one_byte(Fichier,Screen_height&0xFF);
      Write_one_byte(Fichier,Screen_height>>8);
      // Ecriture de la back-color
      Write_one_byte(Fichier,2);
      Write_one_byte(Fichier,1);
      Write_one_byte(Fichier,Back_color);

      // Routine de compression PKM de l'image
      image_size=(dword)(Main_image_width*Main_image_height);
      Compteur_de_pixels=0;
      pixel_value=Read_pixel_old(0,0);

      while ( (Compteur_de_pixels<image_size) && (!File_error) )
      {
        Compteur_de_pixels++;
        repetitions=1;
        last_color=pixel_value;
        pixel_value=Read_pixel_old(Compteur_de_pixels % Main_image_width,Compteur_de_pixels / Main_image_width);
        while ( (pixel_value==last_color)
             && (Compteur_de_pixels<image_size)
             && (repetitions<65535) )
        {
	  repetitions++;
	  Compteur_de_pixels++;
          pixel_value=Read_pixel_old(Compteur_de_pixels % Main_image_width,Compteur_de_pixels / Main_image_width);
        }

        if ( (last_color!=Head.recog1) && (last_color!=Head.recog2) )
        {
	  if (repetitions==1)
            Write_one_byte(Fichier,last_color);
          else
	  if (repetitions==2)
	  {
	    Write_one_byte(Fichier,last_color);
	    Write_one_byte(Fichier,last_color);
	  }
          else
	  if ( (repetitions>2) && (repetitions<256) )
	  { // RECON1/couleur/nombre
	    Write_one_byte(Fichier,Head.recog1);
	    Write_one_byte(Fichier,last_color);
	    Write_one_byte(Fichier,repetitions&0xFF);
	  }
          else
	  if (repetitions>=256)
	  { // RECON2/couleur/hi(nombre)/lo(nombre)
	    Write_one_byte(Fichier,Head.recog2);
	    Write_one_byte(Fichier,last_color);
	    Write_one_byte(Fichier,repetitions>>8);
	    Write_one_byte(Fichier,repetitions&0xFF);
	  }
        }
        else
        {
	  if (repetitions<256)
          {
	    Write_one_byte(Fichier,Head.recog1);
	    Write_one_byte(Fichier,last_color);
	    Write_one_byte(Fichier,repetitions&0xFF);
	  }
	  else
	  {
	    Write_one_byte(Fichier,Head.recog2);
	    Write_one_byte(Fichier,last_color);
	    Write_one_byte(Fichier,repetitions>>8);
	    Write_one_byte(Fichier,repetitions&0xFF);
	  }
        }
      }

      End_write(Fichier);
    }
    else
      File_error=1;
    close(Fichier);
  }
  else
  {
    File_error=1;
    close(Fichier);
  }
  //   S'il y a eu une erreur de sauvegarde, on ne va tout de m�me pas laisser
  // ce fichier pourri tra�ner... Ca fait pas propre.
  if (File_error)
    remove(filename);
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// LBM ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format LBM --------------------------------

  int  LBM_file;

  // ------------ Lire et traduire une valeur au format Motorola ------------
  dword Lire_long(void)
  {
    dword temp;
    dword Valeur;

    if (read(LBM_file,&Valeur,4)!=4)
      File_error=1;
    swab((char *)&Valeur,(char *)&temp,4);
    return ((temp>>16)+(temp<<16));
  }


void Test_LBM(void)
{
  char  filename[256];
  char  Format[4];
  char  Section[4];


  filename_complet(filename,0);

  File_error=0;

  if ((LBM_file=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (read(LBM_file,Section,4)!=4)
      File_error=1;
    else
    if (memcmp(Section,"FORM",4))
      File_error=1;
    else
    {
      Lire_long(); //   On aurait pu v�rifier que ce long est �gal � la taille
                   // du fichier - 8, mais �a aurait interdit de charger des
                   // fichiers tronqu�s (et d�j� que c'est chiant de perdre
                   // une partie du fichier il faut quand m�me pouvoir en
                   // garder un peu... Sinon, moi je pleure :'( !!! )
      if (read(LBM_file,Format,4)!=4)
        File_error=1;
      else
      if ( (memcmp(Format,"ILBM",4)) && (memcmp(Format,"PBM ",4)) )
        File_error=1;
    }
    close(LBM_file);
  }
  else
    File_error=1;
}


// -- Lire un fichier au format LBM -----------------------------------------

  byte * LBM_buffer;
  byte Image_HAM;
  byte HBPm1; // Header.BitPlanes-1

  // ---------------- Adapter la palette pour les images HAM ----------------
  void Adapt_palette_HAM(void)
  {
    short I,J,temp;
    byte  color;

    if (Image_HAM==6)
    {
      for (I=1; I<=14; I++)
      {
        // On recopie a palette de base
        memcpy(Main_palette+(I<<4),Main_palette,48);
        // On modifie les teintes de cette palette
        for (J=0; J<16; J++)
        {
          color=(I<<4)+J;
          if (I<=7)
          {
            if (I&1)
            {
              temp=Main_palette[J].R+16;
              Main_palette[color].R=(temp<63)?temp:63;
            }
            if (I&2)
            {
              temp=Main_palette[J].V+16;
              Main_palette[color].V=(temp<63)?temp:63;
            }
            if (I&4)
            {
              temp=Main_palette[J].B+16;
              Main_palette[color].B=(temp<63)?temp:63;
            }
          }
          else
          {
            if ((I-7)&1)
            {
              temp=Main_palette[J].R-16;
              Main_palette[color].R=(temp>=0)?temp:0;
            }
            if ((I-7)&2)
            {
              temp=Main_palette[J].V-16;
              Main_palette[color].V=(temp>=0)?temp:0;
            }
            if ((I-7)&4)
            {
              temp=Main_palette[J].B-16;
              Main_palette[color].B=(temp>=0)?temp:0;
            }
          }
        }
      }
      // Ici, il reste les 16 derni�res couleurs � modifier
      for (I=240,J=0; J<16; I++,J++)
      {
        temp=Main_palette[J].R+8;
        Main_palette[I].R=(temp<63)?temp:63;
        temp=Main_palette[J].V+8;
        Main_palette[I].V=(temp<63)?temp:63;
        temp=Main_palette[J].B+8;
        Main_palette[I].B=(temp<63)?temp:63;
      }
    }
    else if (Image_HAM==8)
    {
      for (I=1; I<=3; I++)
      {
        // On recopie la palette de base
        memcpy(Main_palette+(I<<6),Main_palette,192);
        // On modifie les teintes de cette palette
        for (J=0; J<64; J++)
        {
          color=(I<<6)+J;
          switch (I)
          {
            case 1 :
              temp=Main_palette[J].R+16;
              Main_palette[color].R=(temp<63)?temp:63;
              break;
            case 2 :
              temp=Main_palette[J].V+16;
              Main_palette[color].V=(temp<63)?temp:63;
              break;
            default:
              temp=Main_palette[J].B+16;
              Main_palette[color].B=(temp<63)?temp:63;
          }
        }
      }
    }
    else // Image 64 couleurs sauv�e en 32.
    {
      for (I=0; I<32; I++)
      {
        J=I+32;
        Main_palette[J].R=Main_palette[I].R>>1;
        Main_palette[J].V=Main_palette[I].V>>1;
        Main_palette[J].B=Main_palette[I].B>>1;
      }
    }
  }

  // ------------------------- Attendre une section -------------------------
  byte Wait_for(byte * expected_section)
  {
    // Valeur retourn�e: 1=Section trouv�e, 0=Section non trouv�e (erreur)
    dword Taille_section;
    byte section_read[4];

    if (read(LBM_file,section_read,4)!=4)
      return 0;
    while (memcmp(section_read,expected_section,4)) // Sect. pas encore trouv�e
    {
      Taille_section=Lire_long();
      if (File_error)
        return 0;
      if (Taille_section&1)
        Taille_section++;
      if (lseek(LBM_file,Taille_section,SEEK_CUR)==-1)
        return 0;
      if (read(LBM_file,section_read,4)!=4)
        return 0;
    }
    return 1;
  }

  // ----------------------- Afficher une ligne ILBM ------------------------
  void Draw_ILBM_line(short Pos_Y, short real_line_size)
  {
    byte  color;
    byte  red,green,blue;
    byte  temp;
    short Pos_X;

    if (Image_HAM<=1)                                               // ILBM
    {
      for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
        Pixel_load_function(Pos_X,Pos_Y,Color_ILBM_line(Pos_X,real_line_size));
    }
    else
    {
      color=0;
      red=Main_palette[0].R;
      green =Main_palette[0].V;
      blue =Main_palette[0].B;
      if (Image_HAM==6)
      for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)         // HAM6
      {
        temp=Color_ILBM_line(Pos_X,real_line_size);
        switch (temp & 0xF0)
        {
          case 0x10: // blue
            blue=(temp&0x0F)<<2;
            color=Best_color(red,green,blue);
            break;
          case 0x20: // red
            red=(temp&0x0F)<<2;
            color=Best_color(red,green,blue);
            break;
          case 0x30: // green
            green=(temp&0x0F)<<2;
            color=Best_color(red,green,blue);
            break;
          default:   // Nouvelle couleur
            color=temp;
            red=Main_palette[color].R;
            green =Main_palette[color].V;
            blue =Main_palette[color].B;
        }
        Pixel_load_function(Pos_X,Pos_Y,color);
      }
      else
      for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)         // HAM8
      {
        temp=Color_ILBM_line(Pos_X,real_line_size);
        switch (temp & 0x03)
        {
          case 0x01: // blue
            blue=temp>>2;
            color=Best_color(red,green,blue);
            break;
          case 0x02: // red
            red=temp>>2;
            color=Best_color(red,green,blue);
            break;
          case 0x03: // green
            green=temp>>2;
            color=Best_color(red,green,blue);
            break;
          default:   // Nouvelle couleur
            color=temp;
            red=Main_palette[color].R;
            green =Main_palette[color].V;
            blue =Main_palette[color].B;
        }
        Pixel_load_function(Pos_X,Pos_Y,color);
      }
    }
  }


void Load_LBM(void)
{
  char  filename[256];
  int   Fichier;
  struct Header_LBM
  {
    word  Width;
    word  Height;
    short X_org;       // Inutile
    short Y_org;       // Inutile
    byte  Bit_planes;
    byte  Mask;
    byte  Compression;
    byte  Pad1;       // Inutile
    word  Transp_col;
    byte  X_aspect;    // Inutile
    byte  Y_aspect;    // Inutile
    short X_screen;
    short Y_screen;
  } Header;
  char  Format[4];
  char  Section[4];
  byte  Octet;
  short b256;
  dword nb_colors;
  dword image_size;
  short Pos_X;
  short Pos_Y;
  short Compteur;
  short line_size;       // Taille d'une ligne en octets
  short real_line_size; // Taille d'une ligne en pixels
  byte  color;
  long  file_size;


  filename_complet(filename,0);

  File_error=0;

  if ((LBM_file=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(LBM_file);

    // On avance dans le fichier (pas besoin de tester ce qui l'a d�j� �t�)
    read(LBM_file,Section,4);
    Lire_long();
    read(LBM_file,Format,4);
    if (!Wait_for("BMHD"))
      File_error=1;
    Lire_long();

    // Maintenant on lit le header pour pouvoir commencer le chargement de l'image
    if ( (read(LBM_file,&Header,sizeof(struct Header_LBM))==sizeof(struct Header_LBM))
      && Header.Width && Header.Height)
    {
      if ( (Header.Bit_planes) && (Wait_for("CMAP")) )
      {
        nb_colors=Lire_long()/3;

        if (((int)1<<Header.Bit_planes)!=nb_colors)
        {
          if ((nb_colors==32) && (Header.Bit_planes==6))
          {              // Ce n'est pas une image HAM mais une image 64 coul.
            Image_HAM=1; // Sauv�e en 32 coul. => il faut copier les 32 coul.
          }              // sur les 32 suivantes et assombrir ces derni�res.
          else
          {
            if ((Header.Bit_planes==6) || (Header.Bit_planes==8))
              Image_HAM=Header.Bit_planes;
            else
              // File_error=1; /* C'est cens� �tre incorrect mais j'ai */
              Image_HAM=0;         /* trouv� un fichier comme �a, alors... */
          }
        }
        else
          Image_HAM=0;

        if ( (!File_error) && (nb_colors>=2) && (nb_colors<=256) )
        {
          HBPm1=Header.Bit_planes-1;
          if (Header.Mask==1)
            Header.Bit_planes++;

          // Deluxe paint le fait... alors on le fait...
          Back_color=Header.Transp_col;

          // On commence par passer la palette en 256 comme �a, si la nouvelle
          // palette a moins de 256 coul, la pr�c�dente ne souffrira pas d'un
          // assombrissement pr�judiciable.
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));
          else
            Palette_64_to_256(Main_palette);
          //   On peut maintenant charger la nouvelle palette
          if (read(LBM_file,Main_palette,3*nb_colors)==(3*nb_colors))
          {
            Palette_256_to_64(Main_palette);
            if (Image_HAM)
              Adapt_palette_HAM();
            Set_palette(Main_palette);
            Remap_fileselector();

            // On lit l'octet de padding du CMAP si la taille est impaire
            if (nb_colors&1)
              if (read(LBM_file,&Octet,1)==1)
                File_error=2;

            if ( (Wait_for("BODY")) && (!File_error) )
            {
              image_size=Lire_long();
              swab((char *)&Header.Width ,(char *)&Main_image_width,2);
              swab((char *)&Header.Height,(char *)&Main_image_height,2);

              swab((char *)&Header.X_screen,(char *)&Original_screen_X,2);
              swab((char *)&Header.Y_screen,(char *)&Original_screen_Y,2);

              Init_preview(Main_image_width,Main_image_height,file_size,FORMAT_LBM);
              if (File_error==0)
              {
                if (!memcmp(Format,"ILBM",4))    // "ILBM": InterLeaved BitMap
                {
                  // Calcul de la taille d'une ligne ILBM (pour les images ayant des dimensions exotiques)
                  if (Main_image_width & 15)
                  {
                    real_line_size=( (Main_image_width+16) >> 4 ) << 4;
                    line_size=( (Main_image_width+16) >> 4 )*(Header.Bit_planes<<1);
                  }
                  else
                  {
                    real_line_size=Main_image_width;
                    line_size=(Main_image_width>>3)*Header.Bit_planes;
                  }

                  if (!Header.Compression)
                  {                                           // non compress�
                    LBM_buffer=(byte *)malloc(line_size);
                    for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
                    {
                      if (read(LBM_file,LBM_buffer,line_size)==line_size)
                        Draw_ILBM_line(Pos_Y,real_line_size);
                      else
                        File_error=2;
                    }
                    free(LBM_buffer);
                  }
                  else
                  {                                               // compress�
                    Init_lecture();

                    LBM_buffer=(byte *)malloc(line_size);

                    for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
                    {
                      for (Pos_X=0; ((Pos_X<line_size) && (!File_error)); )
                      {
                        Octet=Read_one_byte(LBM_file);
                        if (Octet>127)
                        {
                          color=Read_one_byte(LBM_file);
                          b256=(short)(256-Octet);
                          for (Compteur=0; Compteur<=b256; Compteur++)
                            if (Pos_X<line_size)
                              LBM_buffer[Pos_X++]=color;
                            else
                              File_error=2;
                        }
                        else
                          for (Compteur=0; Compteur<=(short)(Octet); Compteur++)
                            if (Pos_X<line_size)
                              LBM_buffer[Pos_X++]=Read_one_byte(LBM_file);
                            else
                              File_error=2;
                      }
                      if (!File_error)
                        Draw_ILBM_line(Pos_Y,real_line_size);
                    }

                    free(LBM_buffer);
                    Close_lecture();
                  }
                }
                else                               // "PBM ": Planar(?) BitMap
                {
                  real_line_size=Main_image_width+(Main_image_width&1);

                  if (!Header.Compression)
                  {                                           // non compress�
                    LBM_buffer=(byte *)malloc(real_line_size);
                    for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
                    {
                      if (read(LBM_file,LBM_buffer,real_line_size)==real_line_size)
                        for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
                          Pixel_load_function(Pos_X,Pos_Y,LBM_buffer[Pos_X]);
                      else
                        File_error=2;
                    }
                    free(LBM_buffer);
                  }
                  else
                  {                                               // compress�
                    Init_lecture();
                    for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
                    {
                      for (Pos_X=0; ((Pos_X<real_line_size) && (!File_error)); )
                      {
                        Octet=Read_one_byte(LBM_file);
                        if (Octet>127)
                        {
                          color=Read_one_byte(LBM_file);
                          b256=256-Octet;
                          for (Compteur=0; Compteur<=b256; Compteur++)
                            Pixel_load_function(Pos_X++,Pos_Y,color);
                        }
                        else
                          for (Compteur=0; Compteur<=Octet; Compteur++)
                            Pixel_load_function(Pos_X++,Pos_Y,Read_one_byte(LBM_file));
                      }
                    }
                    Close_lecture();
                  }
                }
              }
            }
            else
              Set_file_error(2);
          }
          else
          {
            //   On restore l'ancienne palette en cas d'erreur...
            Palette_256_to_64(Main_palette);
            // ... ce qui permet de ne renvoyer qu'une erreur 1 (pas de modif)
            File_error=1;
          }
        }
        else
          Set_file_error(1);
      }
      else
        File_error=1;
    }
    else
      File_error=1;

    close(LBM_file);
  }
  else
    File_error=1;
}


// -- Sauver un fichier au format LBM ---------------------------------------

  byte LBM_color_list[129];
  word LBM_list_size;
  byte LBM_repetition_mode;

  // ----------- Traduire et �crire une valeur au format Motorola -----------
  void Ecrire_long(dword Valeur)
  {
    dword temp;

    swab((char *)&Valeur,(char *)&temp,4);
    Valeur=(temp>>16)+(temp<<16);
    if (write(LBM_file,&Valeur,4)==-1)
      File_error=1;
  }

  // ------------- Ecrire les couleurs que l'on vient de traiter ------------
  void Transfer_colors(void)
  {
    byte index;

    if (LBM_list_size>0)
    {
      if (LBM_repetition_mode)
      {
        Write_one_byte(LBM_file,257-LBM_list_size);
	Write_one_byte(LBM_file,LBM_color_list[0]);
      }
      else
      {
        Write_one_byte(LBM_file,LBM_list_size-1);
        for (index=0; index<LBM_list_size; index++)
          Write_one_byte(LBM_file,LBM_color_list[index]);
      }
    }
    LBM_list_size=0;
    LBM_repetition_mode=0;
  }

  // - Compresion des couleurs encore plus performante que DP2e et que VPIC -
  void New_color(byte color)
  {
    byte last_color;
    byte second_last_color;

    switch (LBM_list_size)
    {
      case 0 : // Premi�re couleur
        LBM_color_list[0]=color;
        LBM_list_size=1;
        break;
      case 1 : // Deuxi�me couleur
        last_color=LBM_color_list[0];
        LBM_repetition_mode=(last_color==color);
        LBM_color_list[1]=color;
        LBM_list_size=2;
        break;
      default: // Couleurs suivantes
        last_color      =LBM_color_list[LBM_list_size-1];
        second_last_color=LBM_color_list[LBM_list_size-2];
        if (last_color==color)  // On a une r�p�tition de couleur
        {
          if ( (LBM_repetition_mode) || (second_last_color!=color) )
          // On conserve le mode...
          {
            LBM_color_list[LBM_list_size]=color;
	    LBM_list_size++;
            if (LBM_list_size==128)
              Transfer_colors();
          }
          else // On est en mode <> et on a 3 couleurs qui se suivent
          {
            LBM_list_size-=2;
            Transfer_colors();
            LBM_color_list[0]=color;
            LBM_color_list[1]=color;
            LBM_color_list[2]=color;
            LBM_list_size=3;
            LBM_repetition_mode=1;
          }
        }
        else // La couleur n'est pas la m�me que la pr�c�dente
        {
          if (!LBM_repetition_mode)                 // On conserve le mode...
          {
            LBM_color_list[LBM_list_size++]=color;
            if (LBM_list_size==128)
              Transfer_colors();
          }
          else                                        // On change de mode...
          {
            Transfer_colors();
            LBM_color_list[LBM_list_size]=color;
            LBM_list_size++;
          }
        }
    }
  }


void Save_LBM(void)
{
  char filename[256];
  struct Header_LBM
  {
    word  Width;
    word  Height;
    short X_org;       // Inutile
    short Y_org;       // Inutile
    byte  BitPlanes;
    byte  Mask;
    byte  Compression;
    byte  Pad1;       // Inutile
    word  Transp_col; // Inutile
    byte  X_aspect;    // Inutile
    byte  Y_aspect;    // Inutile
    short X_screen;
    short Y_screen;
  } Header;
  word Pos_X;
  word Pos_Y;
  byte Octet;
  word real_width;


  File_error=0;
  filename_complet(filename,0);

  // Ouverture du fichier
  LBM_file=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (LBM_file!=-1)
  {
    write(LBM_file,"FORM",4);
    Ecrire_long(0); // On mettra la taille � jour � la fin

    write(LBM_file,"PBM BMHD",8);
    Ecrire_long(20);

    // On corrige la largeur de l'image pour qu'elle soit multiple de 2
    real_width=Main_image_width+(Main_image_width&1);

    //swab((byte *)&real_width,(byte *)&Header.Width,2);
    swab((byte *)&Main_image_width,(byte *)&Header.Width,2);
    swab((byte *)&Main_image_height,(byte *)&Header.Height,2);
    Header.X_org=0;
    Header.Y_org=0;
    Header.BitPlanes=8;
    Header.Mask=0;
    Header.Compression=1;
    Header.Pad1=0;
    Header.Transp_col=Back_color;
    Header.X_aspect=1;
    Header.Y_aspect=1;
    swab((byte *)&Screen_width,(byte *)&Header.X_screen,2);
    swab((byte *)&Screen_height,(byte *)&Header.Y_screen,2);

    write(LBM_file,&Header,sizeof(struct Header_LBM));

    write(LBM_file,"CMAP",4);
    Ecrire_long(sizeof(T_Palette));

    Palette_64_to_256(Main_palette);
    write(LBM_file,Main_palette,sizeof(T_Palette));
    Palette_256_to_64(Main_palette);

    write(LBM_file,"BODY",4);
    Ecrire_long(0); // On mettra la taille � jour � la fin

    Init_write_buffer();

    LBM_list_size=0;

    for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
    {
      for (Pos_X=0; ((Pos_X<real_width) && (!File_error)); Pos_X++)
        New_color(Read_pixel_old(Pos_X,Pos_Y));

      if (!File_error)
        Transfer_colors();
    }

    End_write(LBM_file);
    close(LBM_file);

    if (!File_error)
    {
      LBM_file=open(filename,O_RDWR|O_BINARY);

      lseek(LBM_file,820,SEEK_SET);
      Ecrire_long(filelength(LBM_file)-824);

      if (!File_error)
      {
        lseek(LBM_file,4,SEEK_SET);

        //   Si la taille de la section de l'image (taille fichier-8) est
        // impaire, on rajoute un 0 (Padding) � la fin.
        if (filelength(LBM_file) & 1)
        {
          Ecrire_long(filelength(LBM_file)-7);
          lseek(LBM_file,0,SEEK_END);
          Octet=0;
          if (write(LBM_file,&Octet,1)==-1)
            File_error=1;
        }
        else
          Ecrire_long(filelength(LBM_file)-8);

        close(LBM_file);

        if (File_error)
          remove(filename);
      }
      else
      {
        File_error=1;
        close(LBM_file);
        remove(filename);
      }
    }
    else // Il y a eu une erreur lors du compactage => on efface le fichier
      remove(filename);
  }
  else
    File_error=1;
}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// BMP ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format BMP --------------------------------
void Test_BMP(void)
{
  char filename[256];
  int  Fichier;
  struct BMP_Header
  {
    word Signature;   // ='BM' = 0x4D42
    long Size_1;    // =Taille du fichier
    word Reserved_1;    // =0
    word Reserved_2;    // =0
    long Offset;    // Nb octets avant les donn�es bitmap

    long Size_2;    // =40
    long Largeur;
    long Hauteur;
    word Planes;       // =1
    word Nb_bits;     // =1,4,8 ou 24
    long Compression;
    long Size_3;
    long XPM;
    long YPM;
    long Nb_Clr;
    long Clr_Imprt;
  } Header;


  File_error=1;
  filename_complet(filename,0);

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (read(Fichier,&Header,sizeof(struct BMP_Header))==sizeof(struct BMP_Header))
      if ( (Header.Signature==0x4D42) && (Header.Size_2==40)
        && Header.Largeur && Header.Hauteur )
        File_error=0;
    close(Fichier);
  }
}


// -- Charger un fichier au format BMP --------------------------------------
void Load_BMP(void)
{
  char filename[256];
  int  Fichier;
  struct BMP_Header
  {
    word Signature;   // ='BM' = 0x4D42
    long Size_1;    // =Taille du fichier
    word Reserved_1;    // =0
    word Reserved_2;    // =0
    long Offset;    // Nb octets avant les donn�es bitmap

    long Size_2;    // =40
    long Largeur;
    long Hauteur;
    word Planes;       // =1
    word Nb_bits;     // =1,4,8 ou 24
    long Compression;
    long Size_3;
    long XPM;
    long YPM;
    long Nb_Clr;
    long Clr_Imprt;
  } Header;
  byte * buffer;
  word  index;
  byte  local_palette[256][4]; // R,V,B,0
  word  nb_colors;
  short Pos_X;
  short Pos_Y;
  word  line_size;
  byte  A,B,C;
  long  file_size;


  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);

    if (read(Fichier,&Header,sizeof(struct BMP_Header))==sizeof(struct BMP_Header))
    {
      switch (Header.Nb_bits)
      {
        case 1 :
        case 4 :
        case 8 :
          if (Header.Nb_Clr)
            nb_colors=Header.Nb_Clr;
          else
            nb_colors=1<<Header.Nb_bits;
          break;
        default:
          File_error=1;
      }

      if (!File_error)
      {
        Init_preview(Header.Largeur,Header.Hauteur,file_size,FORMAT_BMP);
        if (File_error==0)
        {
          if (read(Fichier,local_palette,nb_colors<<2)==(nb_colors<<2))
          {
            //   On commence par passer la palette en 256 comme �a, si la nouvelle
            // palette a moins de 256 coul, la pr�c�dente ne souffrira pas d'un
            // assombrissement pr�judiciable.
            if (Config.Clear_palette)
              memset(Main_palette,0,sizeof(T_Palette));
            else
              Palette_64_to_256(Main_palette);
            //   On peut maintenant transf�rer la nouvelle palette
            for (index=0; index<nb_colors; index++)
            {
              Main_palette[index].R=local_palette[index][2];
              Main_palette[index].V=local_palette[index][1];
              Main_palette[index].B=local_palette[index][0];
            }
            Palette_256_to_64(Main_palette);
            Set_palette(Main_palette);
            Remap_fileselector();

            Main_image_width=Header.Largeur;
            Main_image_height=Header.Hauteur;

            switch (Header.Compression)
            {
              case 0 : // Pas de compression
                line_size=Main_image_width;
                Pos_X=(32/Header.Nb_bits); // Pos_X sert de variable temporaire
                if (line_size % Pos_X)
                  line_size=((line_size/Pos_X)*Pos_X)+Pos_X;
                line_size=(line_size*Header.Nb_bits)>>3;

                buffer=(byte *)malloc(line_size);
                for (Pos_Y=Main_image_height-1; ((Pos_Y>=0) && (!File_error)); Pos_Y--)
                {
                  if (read(Fichier,buffer,line_size)==line_size)
                    for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
                      switch (Header.Nb_bits)
                      {
                        case 8 :
                          Pixel_load_function(Pos_X,Pos_Y,buffer[Pos_X]);
                          break;
                        case 4 :
                          if (Pos_X & 1)
                            Pixel_load_function(Pos_X,Pos_Y,buffer[Pos_X>>1] & 0xF);
                          else
                            Pixel_load_function(Pos_X,Pos_Y,buffer[Pos_X>>1] >> 4 );
                          break;
                        case 1 :
                          if ( buffer[Pos_X>>3] & (0x80>>(Pos_X&7)) )
                            Pixel_load_function(Pos_X,Pos_Y,1);
                          else
                            Pixel_load_function(Pos_X,Pos_Y,0);
                      }
                  else
                    File_error=2;
                }
                free(buffer);
                break;

              case 1 : // Compression RLE 8 bits
                Pos_X=0;
                Pos_Y=Main_image_height-1;

                Init_lecture();
                A=Read_one_byte(Fichier);
                B=Read_one_byte(Fichier);
                while ( (!File_error) && ((A)||(B!=1)) )
                {
                  if (A) // Encoded mode
                    for (index=1; index<=A; index++)
                      Pixel_load_function(Pos_X++,Pos_Y,B);
                  else   // Absolute mode
                    switch (B)
                    {
                      case 0 : // End of line
                        Pos_X=0;
                        Pos_Y--;
                        break;
                      case 1 : // End of bitmap
                        break;
                      case 2 : // Delta
                        A=Read_one_byte(Fichier);
                        B=Read_one_byte(Fichier);
                        Pos_X+=A;
                        Pos_Y-=B;
                        break;
                      default: // Nouvelle s�rie
                        while (B)
                        {
                          A=Read_one_byte(Fichier);
                          C=Read_one_byte(Fichier);
                          Pixel_load_function(Pos_X++,Pos_Y,A);
                          if (--B)
                          {
                            Pixel_load_function(Pos_X++,Pos_Y,C);
                            B--;
                          }
                        }
                    }
                  A=Read_one_byte(Fichier);
                  B=Read_one_byte(Fichier);
                }
                Close_lecture();
                break;

              case 2 : // Compression RLE 4 bits
                Pos_X=0;
                Pos_Y=Main_image_height-1;

                Init_lecture();
                A=Read_one_byte(Fichier);
                B=Read_one_byte(Fichier);
                while ( (!File_error) && ((A)||(B!=1)) )
                {
                  if (A) // Encoded mode (A fois les 1/2 pixels de B)
                    for (index=1; index<=A; index++)
                    {
                      if (index & 1)
                        Pixel_load_function(Pos_X,Pos_Y,B>>4);
                      else
                        Pixel_load_function(Pos_X,Pos_Y,B&0xF);
                      Pos_X++;
                    }
                  else   // Absolute mode
                    switch (B)
                    {
                      case 0 : //End of line
                        Pos_X=0;
                        Pos_Y--;
                        break;
                      case 1 : // End of bitmap
                        break;
                      case 2 : // Delta
                        A=Read_one_byte(Fichier);
                        B=Read_one_byte(Fichier);
                        Pos_X+=A;
                        Pos_Y-=B;
                        break;
                      default: // Nouvelle s�rie (B 1/2 pixels bruts)
                        for (index=1; ((index<=B) && (!File_error)); index++,Pos_X++)
                        {
                          if (index&1)
                          {
                            C=Read_one_byte(Fichier);
                            Pixel_load_function(Pos_X,Pos_Y,C>>4);
                          }
                          else
                            Pixel_load_function(Pos_X,Pos_Y,C&0xF);
                        }
                        //   On lit l'octet rendant le nombre d'octets pair, si
                        // n�cessaire. Encore un truc de cr�tin "made in MS".
                        if ( ((B&3)==1) || ((B&3)==2) )
                          Read_one_byte(Fichier);
                    }
                  A=Read_one_byte(Fichier);
                  B=Read_one_byte(Fichier);
                }
                Close_lecture();
            }
            close(Fichier);
          }
          else
          {
            close(Fichier);
            File_error=1;
          }
        }
      }
      else
      {
        // Image 24 bits!!!
        File_error=0;

        Main_image_width=Header.Largeur;
        Main_image_height=Header.Hauteur;
        Init_preview(Header.Largeur,Header.Hauteur,file_size,FORMAT_BMP | FORMAT_24B);

        if (File_error==0)
        {
          line_size=Main_image_width*3;
          Pos_X=(line_size % 4); // Pos_X sert de variable temporaire
          if (Pos_X>0)
            line_size+=(4-Pos_X);

          buffer=(byte *)malloc(line_size);
          for (Pos_Y=Main_image_height-1; ((Pos_Y>=0) && (!File_error)); Pos_Y--)
          {
            if (read(Fichier,buffer,line_size)==line_size)
              for (Pos_X=0,index=0; Pos_X<Main_image_width; Pos_X++,index+=3)
                Pixel_load_24b(Pos_X,Pos_Y,buffer[index+2],buffer[index+1],buffer[index+0]);
            else
              File_error=2;
          }
          free(buffer);
          close(Fichier);
        }
      }
    }
    else
    {
      close(Fichier);
      File_error=1;
    }
  }
  else
    File_error=1;
}


// -- Sauvegarder un fichier au format BMP ----------------------------------
void Save_BMP(void)
{
  char filename[256];
  int  Fichier;
  struct BMP_Header
  {
    word Signature;   // ='BM' = 0x4D42
    long Size_1;    // =Taille du fichier
    word Reserved_1;    // =0
    word Reserved_2;    // =0
    long Offset;    // Nb octets avant les donn�es bitmap

    long Size_2;    // =40
    long Largeur;
    long Hauteur;
    word Planes;       // =1
    word Nb_bits;     // =1,4,8 ou 24
    long Compression;
    long Size_3;
    long XPM;
    long YPM;
    long Nb_Clr;
    long Clr_Imprt;
  } Header;
  short Pos_X;
  short Pos_Y;
  long line_size;
  word index;
  byte local_palette[256][4]; // R,V,B,0


  File_error=0;
  filename_complet(filename,0);

  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    if (Main_image_width & 7)
      line_size=((Main_image_width >> 3)+1) << 3;
    else
      line_size=Main_image_width;

    Header.Signature  =0x4D42;
    Header.Size_1   =(line_size*Main_image_height)+1078;
    Header.Reserved_1   =0;
    Header.Reserved_2   =0;
    Header.Offset   =1078;
    Header.Size_2   =40;
    Header.Largeur    =line_size;
    Header.Hauteur    =Main_image_height;
    Header.Planes      =1;
    Header.Nb_bits    =8;
    Header.Compression=0;
    Header.Size_3   =0;
    Header.XPM        =0;
    Header.YPM        =0;
    Header.Nb_Clr     =0;
    Header.Clr_Imprt  =0;

    if (write(Fichier,&Header,sizeof(struct BMP_Header))!=-1)
    {
      //   Chez Bill, ils ont dit: "On va mettre les couleur dans l'ordre
      // inverse, et pour faire chier, on va les mettre sur une �chelle de
      // 0 � 255 parce que le standard VGA c'est de 0 � 63 (logique!). Et
      // puis comme c'est pas assez d�bile, on va aussi y rajouter un octet
      // toujours � 0 pour forcer les gens � s'acheter des gros disques
      // durs... Comme �a, �a fera passer la pillule lorsqu'on sortira
      // Windows 95." ...
      Palette_64_to_256(Main_palette);
      for (index=0; index<256; index++)
      {
        local_palette[index][0]=Main_palette[index].B;
        local_palette[index][1]=Main_palette[index].V;
        local_palette[index][2]=Main_palette[index].R;
        local_palette[index][3]=0;
      }
      Palette_256_to_64(Main_palette);

      if (write(Fichier,local_palette,1024)!=-1)
      {
        Init_write_buffer();

        // ... Et Bill, il a dit: "OK les gars! Mais seulement si vous rangez
        // les pixels dans l'ordre inverse, mais que sur les Y quand-m�me
        // parce que faut pas pousser."
        for (Pos_Y=Main_image_height-1; ((Pos_Y>=0) && (!File_error)); Pos_Y--)
          for (Pos_X=0; Pos_X<line_size; Pos_X++)
            Write_one_byte(Fichier,Read_pixel_old(Pos_X,Pos_Y));

        End_write(Fichier);
        close(Fichier);

        if (File_error)
          remove(filename);
      }
      else
      {
        close(Fichier);
        remove(filename);
        File_error=1;
      }

    }
    else
    {
      close(Fichier);
      remove(filename);
      File_error=1;
    }
  }
  else
    File_error=1;
}





/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// GIF ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format GIF --------------------------------

void Test_GIF(void)
{
  char filename[256];
  char Signature[6];
  int  Fichier;


  File_error=1;
  filename_complet(filename,0);

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (
        (read(Fichier,Signature,6)==6) &&
        ((!memcmp(Signature,"GIF87a",6))||(!memcmp(Signature,"GIF89a",6)))
       )
      File_error=0;

    close(Fichier);
  }
}


// -- Lire un fichier au format GIF -----------------------------------------

  // D�finition de quelques variables globales au chargement du GIF87a
  word GIF_nb_bits;        // Nb de bits composants un code complet
  word GIF_remainder_bits;      // Nb de bits encore dispos dans GIF_last_byte
  word GIF_remainder_byte;      // Nb d'octets avant le prochain bloc de Raster Data
  word GIF_current_code;    // Code trait� (qui vient d'�tre lu en g�n�ral)
  word GIF_last_byte;      // Octet de lecture des bits
  word GIF_pos_X;          // Coordonn�es d'affichage de l'image
  word GIF_pos_Y;
  word GIF_interlaced;     // L'image est entrelac�e
  word GIF_finished_interlaced_image; // L'image entrelac�e est finie de charger
  word GIF_pass;          // index de passe de l'image entrelac�e
  int  GIF_file;        // L'handle du fichier

  // -- Lit le code � GIF_nb_bits suivant --

  word GIF_get_next_code(void)
  {
    word nb_bits_to_process=GIF_nb_bits;
    word nb_bits_processed  =0;
    word current_nb_bits;

    GIF_current_code=0;

    while (nb_bits_to_process)
    {
      if (GIF_remainder_bits==0) // Il ne reste plus de bits...
      {
        // Lire l'octet suivant:

        // Si on a atteint la fin du bloc de Raster Data
        if (GIF_remainder_byte==0)
          // Lire l'octet nous donnant la taille du bloc de Raster Data suivant
          GIF_remainder_byte=Read_one_byte(GIF_file);
        GIF_last_byte=Read_one_byte(GIF_file);
        GIF_remainder_byte--;
        GIF_remainder_bits=8;
      }

      current_nb_bits=(nb_bits_to_process<=GIF_remainder_bits)?nb_bits_to_process:GIF_remainder_bits;

      GIF_current_code|=(GIF_last_byte & ((1<<current_nb_bits)-1))<<nb_bits_processed;
      GIF_last_byte>>=current_nb_bits;
      nb_bits_processed  +=current_nb_bits;
      nb_bits_to_process-=current_nb_bits;
      GIF_remainder_bits    -=current_nb_bits;
    }

    return GIF_current_code;
  }

  // -- Affiche un nouveau pixel --

  void GIF_new_pixel(byte color)
  {
    Pixel_load_function(GIF_pos_X,GIF_pos_Y,color);

    GIF_pos_X++;

    if (GIF_pos_X>=Main_image_width)
    {
      GIF_pos_X=0;

      if (!GIF_interlaced)
        GIF_pos_Y++;
      else
      {
        switch (GIF_pass)
        {
          case 0 : GIF_pos_Y+=8;
                   break;
          case 1 : GIF_pos_Y+=8;
                   break;
          case 2 : GIF_pos_Y+=4;
                   break;
          default: GIF_pos_Y+=2;
        }

        if (GIF_pos_Y>=Main_image_height)
        {
          switch(++GIF_pass)
          {
          case 1 : GIF_pos_Y=4;
                   break;
          case 2 : GIF_pos_Y=2;
                   break;
          case 3 : GIF_pos_Y=1;
                   break;
          case 4 : GIF_finished_interlaced_image=1;
          }
        }
      }
    }
  }


void Load_GIF(void)
{
  char filename[256];
  char Signature[6];

  word * alphabet_stack;     // Pile de d�codage d'une cha�ne
  word * alphabet_prefix;  // Table des pr�fixes des codes
  word * alphabet_suffix;  // Table des suffixes des codes
  word   alphabet_free;     // Position libre dans l'alphabet
  word   alphabet_max;      // Nombre d'entr�es possibles dans l'alphabet
  word   alphabet_stack_pos; // Position dans la pile de d�codage d'un cha�ne

  struct Type_LSDB
  {
    word Largeur; // Largeur de l'�cran virtuel
    word Hauteur; // Hauteur de l'�cran virtuel
    byte Resol;   // Informations sur la r�solution (et autres)
    byte Backcol; // color de fond
    byte Aspect;  // Informations sur l'aspect ratio (et autres)
  } LSDB; // Logical Screen Descriptor Block

  struct Type_IDB
  {
    word Pos_X;         // Abscisse o� devrait �tre affich�e l'image
    word Pos_Y;         // Ordonn�e o� devrait �tre affich�e l'image
    word Image_width; // Largeur de l'image
    word Image_height; // Hauteur de l'image
    byte Indicator;    // Informations diverses sur l'image
    byte Nb_bits_pixel; // Nb de bits par pixel
  } IDB; // Image Descriptor Block

  word nb_colors;       // Nombre de couleurs dans l'image
  word color_index; // index de traitement d'une couleur
  word size_to_read; // Nombre de donn�es � lire      (divers)
  word Indice_de_lecture; // index de lecture des donn�es (divers)
  byte block_indentifier;  // Code indicateur du type de bloc en cours
  word initial_nb_bits;   // Nb de bits au d�but du traitement LZW
  word special_case;       // M�moire pour le cas sp�cial
  word old_code;       // Code pr�c�dent
  word Read_byte;         // Sauvegarde du code en cours de lecture
  word value_clr;        // Valeur <=> Clear tables
  word value_eof;        // Valeur <=> End d'image
  long file_size;


  /////////////////////////////////////////////////// FIN DES DECLARATIONS //


  GIF_pos_X=0;
  GIF_pos_Y=0;
  GIF_last_byte=0;
  GIF_remainder_bits=0;
  GIF_remainder_byte=0;

  filename_complet(filename,0);

  if ((GIF_file=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(GIF_file);

    if ( (read(GIF_file,Signature,6)==6) &&
         ( (memcmp(Signature,"GIF87a",6)==0) ||
           (memcmp(Signature,"GIF89a",6)==0) ) )
    {
      // Allocation de m�moire pour les tables & piles de traitement:
      alphabet_stack   =(word *)malloc(4096*sizeof(word));
      alphabet_prefix=(word *)malloc(4096*sizeof(word));
      alphabet_suffix=(word *)malloc(4096*sizeof(word));

      if (read(GIF_file,&LSDB,sizeof(struct Type_LSDB))==sizeof(struct Type_LSDB))
      {
        // Lecture du Logical Screen Descriptor Block r�ussie:

        Original_screen_X=LSDB.Largeur;
        Original_screen_Y=LSDB.Hauteur;

        // Palette globale dispo = (LSDB.Resol  and $80)
        // Profondeur de couleur =((LSDB.Resol  and $70) shr 4)+1
        // Nombre de bits/pixel  = (LSDB.Resol  and $07)+1
        // Ordre de Classement   = (LSDB.Aspect and $80)

        alphabet_stack_pos=0;
        GIF_last_byte    =0;
        GIF_remainder_bits    =0;
        GIF_remainder_byte    =0;

        nb_colors=(1 << ((LSDB.Resol & 0x07)+1));
        initial_nb_bits=(LSDB.Resol & 0x07)+2;

        if (LSDB.Resol & 0x80)
        {
          // Palette globale dispo:

          //   On commence par passer la palette en 256 comme �a, si la
          // nouvelle palette a moins de 256 coul, la pr�c�dente ne souffrira
          // pas d'un assombrissement pr�judiciable.
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));
          else
            Palette_64_to_256(Main_palette);

          //   On peut maintenant charger la nouvelle palette:
          if (!(LSDB.Aspect & 0x80))
            // Palette dans l'ordre:
            read(GIF_file,Main_palette,nb_colors*3);
          else
          {
            // Palette tri�e par composantes:
            for (color_index=0;color_index<nb_colors;color_index++)
              read(GIF_file,&Main_palette[color_index].R,1);
            for (color_index=0;color_index<nb_colors;color_index++)
              read(GIF_file,&Main_palette[color_index].V,1);
            for (color_index=0;color_index<nb_colors;color_index++)
              read(GIF_file,&Main_palette[color_index].B,1);
          }

          Palette_256_to_64(Main_palette);
          Set_palette(Main_palette);
        }

        // On s'appr�te � sauter tous les blocks d'extension:

        // On lit un indicateur de block
        read(GIF_file,&block_indentifier,1);
        // Si l'indicateur de block annonce un block d'extension:
        while (block_indentifier==0x21)
        {
          // Lecture du code de fonction:
          read(GIF_file,&block_indentifier,1);

          // On exploitera peut-�tre un jour ce code indicateur pour stocker
          // des remarques dans le fichier. En attendant d'en conna�tre plus
          // on se contente de sauter tous les blocs d'extension:

          // Lecture de la taille du bloc:
          size_to_read=0;
          read(GIF_file,&size_to_read,1);
          while (size_to_read!=0)
          {
            // On saute le bloc:
            lseek(GIF_file,size_to_read,SEEK_CUR);
            // Lecture de la taille du bloc suivant:
            read(GIF_file,&size_to_read,1);
          }

          // Lecture du code de fonction suivant:
          read(GIF_file,&block_indentifier,1);
        }

        if (block_indentifier==0x2C)
        {
          // Pr�sence d'un Image Separator Header

          // lecture de 10 derniers octets
          if ( (read(GIF_file,&IDB,sizeof(struct Type_IDB))==sizeof(struct Type_IDB))
            && IDB.Image_width && IDB.Image_height)
          {
            Main_image_width=IDB.Image_width;
            Main_image_height=IDB.Image_height;

            Init_preview(IDB.Image_width,IDB.Image_height,file_size,FORMAT_GIF);

            // Palette locale dispo = (IDB.Indicator and $80)
            // Image entrelac�e     = (IDB.Indicator and $40)
            // Ordre de classement  = (IDB.Indicator and $20)
            // Nombre de bits/pixel = (IDB.Indicator and $07)+1 (si palette locale dispo)

            if (IDB.Indicator & 0x80)
            {
              // Palette locale dispo

              nb_colors=(1 << ((IDB.Indicator & 0x07)+1));
              initial_nb_bits=(IDB.Indicator & 0x07)+2;

              //   On commence par passer la palette en 256 comme �a, si la
              // nouvelle palette a moins de 256 coul, la pr�c�dente ne
              // souffrira pas d'un assombrissement pr�judiciable.
              Palette_64_to_256(Main_palette);

              if (!(IDB.Indicator & 0x40))
                // Palette dans l'ordre:
                read(GIF_file,Main_palette,nb_colors*3);
              else
              {
                // Palette tri�e par composantes:
                for (color_index=0;color_index<nb_colors;color_index++)
                  read(GIF_file,&Main_palette[color_index].R,1);
                for (color_index=0;color_index<nb_colors;color_index++)
                  read(GIF_file,&Main_palette[color_index].V,1);
                for (color_index=0;color_index<nb_colors;color_index++)
                  read(GIF_file,&Main_palette[color_index].B,1);
              }

              Palette_256_to_64(Main_palette);
              Set_palette(Main_palette);
            }

            Remap_fileselector();

            value_clr   =nb_colors+0;
            value_eof   =nb_colors+1;
            alphabet_free=nb_colors+2;
            GIF_nb_bits  =initial_nb_bits;
            alphabet_max      =((1 <<  GIF_nb_bits)-1);
            GIF_interlaced    =(IDB.Indicator & 0x40);
            GIF_pass         =0;

            Init_lecture();

            File_error=0;
            GIF_finished_interlaced_image=0;

            //////////////////////////////////////////// DECOMPRESSION LZW //

            while ( (GIF_get_next_code()!=value_eof) && (!File_error) )
            {
              if (GIF_current_code<=alphabet_free)
              {
                if (GIF_current_code!=value_clr)
                {
                  if (alphabet_free==(Read_byte=GIF_current_code))
                  {
                    GIF_current_code=old_code;
                    alphabet_stack[alphabet_stack_pos++]=special_case;
                  }

                  while (GIF_current_code>value_clr)
                  {
                    alphabet_stack[alphabet_stack_pos++]=alphabet_suffix[GIF_current_code];
                    GIF_current_code=alphabet_prefix[GIF_current_code];
                  }

                  special_case=alphabet_stack[alphabet_stack_pos++]=GIF_current_code;

                  do
                    GIF_new_pixel(alphabet_stack[--alphabet_stack_pos]);
                  while (alphabet_stack_pos!=0);

                  alphabet_prefix[alphabet_free  ]=old_code;
                  alphabet_suffix[alphabet_free++]=GIF_current_code;
                  old_code=Read_byte;

                  if (alphabet_free>alphabet_max)
                  {
                    if (GIF_nb_bits<12)
                      alphabet_max      =((1 << (++GIF_nb_bits))-1);
                  }
                }
                else // Code Clear rencontr�
                {
                  GIF_nb_bits       =initial_nb_bits;
                  alphabet_max      =((1 <<  GIF_nb_bits)-1);
                  alphabet_free     =nb_colors+2;
                  special_case       =GIF_get_next_code();
                  old_code       =GIF_current_code;
                  GIF_new_pixel(GIF_current_code);
                }
              }
              else
                File_error=2;
            } // Code End-Of-Information ou erreur de fichier rencontr�

            Close_lecture();

            if (File_error>=0)
            if ( /* (GIF_pos_X!=0) || */
                 ( ( (!GIF_interlaced) && (GIF_pos_Y!=Main_image_height) ) ||
                   (  (GIF_interlaced) && (!GIF_finished_interlaced_image) )
                 ) )
              File_error=2;
          } // Le fichier contenait un IDB
          else
            File_error=2;

        } // Le fichier contenait une image
        else
          File_error=2;

      } // Le fichier contenait un LSDB
      else
        File_error=1;

      // Lib�ration de la m�moire utilis�e par les tables & piles de traitement:
      free(alphabet_suffix);
      free(alphabet_prefix);
      free(alphabet_stack);
    } // Le fichier contenait au moins la signature GIF87a ou GIF89a
    else
      File_error=1;

    close(GIF_file);

  } // Le fichier �tait ouvrable
  else
    File_error=1;
}


// -- Sauver un fichier au format GIF ---------------------------------------

  int  GIF_stop;         // "On peut arr�ter la sauvegarde du fichier"
  byte GIF_buffer[256];   // buffer d'�criture de bloc de donn�es compil�es

  // -- Vider le buffer GIF dans le buffer KM --

  void GIF_empty_buffer(void)
  {
    word index;

    if (GIF_remainder_byte)
    {
      GIF_buffer[0]=GIF_remainder_byte;

      for (index=0;index<=GIF_remainder_byte;index++)
        Write_one_byte(GIF_file,GIF_buffer[index]);

      GIF_remainder_byte=0;
    }
  }

  // -- Ecrit un code � GIF_nb_bits --

  void GIF_set_code(word Code)
  {
    word nb_bits_to_process=GIF_nb_bits;
    word nb_bits_processed  =0;
    word current_nb_bits;

    while (nb_bits_to_process)
    {
      current_nb_bits=(nb_bits_to_process<=(8-GIF_remainder_bits))?nb_bits_to_process:(8-GIF_remainder_bits);

      GIF_last_byte|=(Code & ((1<<current_nb_bits)-1))<<GIF_remainder_bits;
      Code>>=current_nb_bits;
      GIF_remainder_bits    +=current_nb_bits;
      nb_bits_processed  +=current_nb_bits;
      nb_bits_to_process-=current_nb_bits;

      if (GIF_remainder_bits==8) // Il ne reste plus de bits � coder sur l'octet courant
      {
        // Ecrire l'octet � balancer:
        GIF_buffer[++GIF_remainder_byte]=GIF_last_byte;

        // Si on a atteint la fin du bloc de Raster Data
        if (GIF_remainder_byte==255)
          // On doit vider le buffer qui est maintenant plein
          GIF_empty_buffer();

        GIF_last_byte=0;
        GIF_remainder_bits=0;
      }
    }
  }


  // -- Lire le pixel suivant --

  byte GIF_next_pixel(void)
  {
    byte temp;

    temp=Read_pixel_old(GIF_pos_X,GIF_pos_Y);

    if (++GIF_pos_X>=Main_image_width)
    {
      GIF_pos_X=0;
      if (++GIF_pos_Y>=Main_image_height)
        GIF_stop=1;
    }

    return temp;
  }



void Save_GIF(void)
{
  char filename[256];

  word * alphabet_prefix;  // Table des pr�fixes des codes
  word * alphabet_suffix;  // Table des suffixes des codes
  word * alphabet_daughter;    // Table des cha�nes filles (plus longues)
  word * alphabet_sister;    // Table des cha�nes soeurs (m�me longueur)
  word   alphabet_free;     // Position libre dans l'alphabet
  word   alphabet_max;      // Nombre d'entr�es possibles dans l'alphabet
  word   start;            // Code pr�c�dent (sert au linkage des cha�nes)
  int    descend;          // Bool�en "On vient de descendre"

  struct Type_LSDB
  {
    word Largeur; // Largeur de l'�cran virtuel |_ Dimensions de l'image si 1
    word Hauteur; // Hauteur de l'�cran virtuel |  seule image dans le fichier (ce qui est notre cas)
    byte Resol;   // Informations sur la r�solution (et autres)
    byte Backcol; // color de fond
    byte Aspect;  // Informations sur l'aspect ratio (et autres)
  } LSDB; // Logical Screen Descriptor Block

  struct Type_IDB
  {
    word Pos_X;         // Abscisse o� devrait �tre affich�e l'image
    word Pos_Y;         // Ordonn�e o� devrait �tre affich�e l'image
    word Image_width; // Largeur de l'image
    word Image_height; // Hauteur de l'image
    byte Indicator;    // Informations diverses sur l'image
    byte Nb_bits_pixel; // Nb de bits par pixel
  } IDB; // Image Descriptor Block

  byte block_indentifier;  // Code indicateur du type de bloc en cours
  word current_string;   // Code de la cha�ne en cours de traitement
  byte Caractere;         // Caract�re � coder
  word index;            // index de recherche de cha�ne


  /////////////////////////////////////////////////// FIN DES DECLARATIONS //


  GIF_pos_X=0;
  GIF_pos_Y=0;
  GIF_last_byte=0;
  GIF_remainder_bits=0;
  GIF_remainder_byte=0;

  filename_complet(filename,0);

  GIF_file=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (GIF_file!=-1)
  {
    // On �crit la signature du fichier
    if (write(GIF_file,"GIF87a",6)==6)
    {
      // La signature du fichier a �t� correctement �crite.

      // Allocation de m�moire pour les tables
      alphabet_prefix=(word *)malloc(4096*sizeof(word));
      alphabet_suffix=(word *)malloc(4096*sizeof(word));
      alphabet_daughter  =(word *)malloc(4096*sizeof(word));
      alphabet_sister  =(word *)malloc(4096*sizeof(word));

      // On initialise le LSDB du fichier
      if (Config.Screen_size_in_GIF)
      {
        LSDB.Largeur=Screen_width;
        LSDB.Hauteur=Screen_height;
      }
      else
      {
        LSDB.Largeur=Main_image_width;
        LSDB.Hauteur=Main_image_height;
      }
      LSDB.Resol  =0x97;          // Image en 256 couleurs, avec une palette
      LSDB.Backcol=0;
      LSDB.Aspect =0;             // Palette normale

      // On sauve le LSDB dans le fichier

      if (write(GIF_file,&LSDB,sizeof(struct Type_LSDB))==sizeof(struct Type_LSDB))
      {
        // Le LSDB a �t� correctement �crit.

        // On sauve la palette

        Palette_64_to_256(Main_palette);
        if (write(GIF_file,Main_palette,768)==768)
        {
          // La palette a �t� correctement �crite.

          //   Le jour o� on se servira des blocks d'extensions pour placer
          // des commentaires, on le fera ici.

          // On va �crire un block indicateur d'IDB et l'IDB du fichier

          block_indentifier=0x2C;
          IDB.Pos_X=0;
          IDB.Pos_Y=0;
          IDB.Image_width=Main_image_width;
          IDB.Image_height=Main_image_height;
          IDB.Indicator=0x07;    // Image non entrelac�e, pas de palette locale.
          IDB.Nb_bits_pixel=8; // Image 256 couleurs;

          if ( (write(GIF_file,&block_indentifier,1)==1) &&
               (write(GIF_file,&IDB,sizeof(struct Type_IDB))==sizeof(struct Type_IDB)) )
          {
            //   Le block indicateur d'IDB et l'IDB ont �t�s correctements
            // �crits.

            Init_write_buffer();

            index=4096;
            File_error=0;
            GIF_stop=0;

            // R�intialisation de la table:
            alphabet_free=258;
            GIF_nb_bits  =9;
            alphabet_max =511;
            GIF_set_code(256);
            for (start=0;start<4096;start++)
            {
              alphabet_daughter[start]=4096;
              alphabet_sister[start]=4096;
            }

            ////////////////////////////////////////////// COMPRESSION LZW //

            start=current_string=GIF_next_pixel();
            descend=1;

            do
            {
              Caractere=GIF_next_pixel();

              //   On regarde si dans la table on aurait pas une cha�ne
              // �quivalente � current_string+Caractere

              while ( (index<alphabet_free) &&
                      ( (current_string!=alphabet_prefix[index]) ||
                        (Caractere      !=alphabet_suffix[index]) ) )
              {
                descend=0;
                start=index;
                index=alphabet_sister[index];
              }

              if (index<alphabet_free)
              {
                //   On sait ici que la current_string+Caractere se trouve
                // en position index dans les tables.

                descend=1;
                start=current_string=index;
                index=alphabet_daughter[index];
              }
              else
              {
                // On fait la jonction entre la current_string et l'actuelle
                if (descend)
                  alphabet_daughter[start]=alphabet_free;
                else
                  alphabet_sister[start]=alphabet_free;

                // On rajoute la cha�ne current_string+Caractere � la table
                alphabet_prefix[alphabet_free  ]=current_string;
                alphabet_suffix[alphabet_free++]=Caractere;

                // On �crit le code dans le fichier
                GIF_set_code(current_string);

                if (alphabet_free>0xFFF)
                {
                  // R�intialisation de la table:
                  GIF_set_code(256);
                  alphabet_free=258;
                  GIF_nb_bits  =9;
                  alphabet_max =511;
                  for (start=0;start<4096;start++)
                  {
                    alphabet_daughter[start]=4096;
                    alphabet_sister[start]=4096;
                  }
                }
                else if (alphabet_free>alphabet_max+1)
                {
                  // On augmente le nb de bits

                  GIF_nb_bits++;
                  alphabet_max=(1<<GIF_nb_bits)-1;
                }

                // On initialise la current_string et le reste pour la suite
                index=alphabet_daughter[Caractere];
                start=current_string=Caractere;
                descend=1;
              }
            }
            while ((!GIF_stop) && (!File_error));

            if (!File_error)
            {
              // On �crit le code dans le fichier
              GIF_set_code(current_string); // Derni�re portion d'image

              //   Cette derni�re portion ne devrait pas poser de probl�mes
              // du c�t� GIF_nb_bits puisque pour que GIF_nb_bits change de
              // valeur, il faudrait que la table de cha�ne soit remplie or
              // c'est impossible puisqu'on traite une cha�ne qui se trouve
              // d�j� dans la table, et qu'elle n'a rien d'in�dit. Donc on
              // ne devrait pas avoir � changer de taille, mais je laisse
              // quand m�me en remarque tout �a, au cas o� il subsisterait
              // des probl�mes dans certains cas exceptionnels.
              //
              // Note: de toutes fa�ons, ces lignes en commentaires ont �t�s
              //      �crites par copier/coller du temps o� la sauvegarde du
              //      GIF d�connait. Il y a donc fort � parier qu'elles ne
              //      sont pas correctes.

              /*
              if (current_string==alphabet_max)
              {
                if (alphabet_max==0xFFF)
                {
                  // On balargue un Clear Code
                  GIF_set_code(256);

                  // On r�initialise les donn�es LZW
                  alphabet_free=258;
                  GIF_nb_bits  =9;
                  alphabet_max =511;
                }
                else
                {
                  GIF_nb_bits++;
                  alphabet_max=(1<<GIF_nb_bits)-1;
                }
              }
              */

              GIF_set_code(257);             // Code de End d'image
              if (GIF_remainder_bits!=0)
                GIF_set_code(0);             // Code bidon permettant de s'assurer que tous les bits du dernier code aient bien �t�s inscris dans le buffer GIF
              GIF_empty_buffer();         // On envoie les derni�res donn�es du buffer GIF dans le buffer KM
              End_write(GIF_file);   // On envoie les derni�res donn�es du buffer KM  dans le fichier

              current_string=0x3B00;        // On �crit un GIF TERMINATOR, exig� par SVGA et SEA.
              if (write(GIF_file,&current_string,sizeof(current_string))!=sizeof(current_string))
                File_error=1;
            }

          } // On a pu �crire l'IDB
          else
            File_error=1;

        } // On a pu �crire la palette
        else
          File_error=1;

        Palette_256_to_64(Main_palette);
      } // On a pu �crire le LSDB
      else
        File_error=1;

      // Lib�ration de la m�moire utilis�e par les tables
      free(alphabet_sister);
      free(alphabet_daughter);
      free(alphabet_suffix);
      free(alphabet_prefix);

    } // On a pu �crire la signature du fichier
    else
      File_error=1;

    close(GIF_file);
    if (File_error)
      remove(filename);

  } // On a pu ouvrir le fichier en �criture
  else
    File_error=1;
}






/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PCX ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format PCX --------------------------------

void Test_PCX(void)
{
  char filename[256];
  int  Fichier;
  struct PCX_header
  {
    byte Manufacturer;       // |_ Il font chier ces cons! Ils auraient pu
    byte Version;            // |  mettre une vraie signature significative!
    byte Compression;        // L'image est-elle compress�e?
    byte Depth;              // Nombre de bits pour coder un pixel (inutile puisqu'on se sert de Plane)
    word X_min;              // |_ Coin haut-gauche   |
    word Y_min;              // |  de l'image         |_ (Cr�tin!)
    word X_max;              // |_ Coin bas-droit     |
    word Y_max;              // |  de l'image         |
    word X_dpi;              // |_ Densit� de |_ (Presque inutile parce que
    word Y_dpi;              // |  l'image    |  aucun moniteur n'est pareil!)
    byte Palette_16c[48];    // Palette 16 coul (inutile pour 256c) (d�bile!)
    byte Reserved;           // Ca me plait �a aussi!
    byte Plane;              // 4 => 16c , 1 => 256c
    word Bytes_per_plane_line;// Doit toujours �tre pair
    word Palette_info;       // 1 => color , 2 => Gris (ignor� � partir de la version 4)
    word Screen_X;           // |_ Dimensions de
    word Screen_Y;           // |  l'�cran d'origine
    byte Filler[54];         // Ca... J'adore!
  } Header; // Je hais ce header!


  File_error=0;
  filename_complet(filename,0);

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (read(Fichier,&Header,sizeof(struct PCX_header))==sizeof(struct PCX_header))
    {
      //   Vu que ce header a une signature de merde et peu significative, il
      // va falloir que je teste diff�rentes petites valeurs dont je connais
      // l'intervalle. Grrr!
      if ( (Header.Manufacturer!=10)
        || (Header.Compression>1)
        || ( (Header.Depth!=1) && (Header.Depth!=2) && (Header.Depth!=4) && (Header.Depth!=8) )
        || ( (Header.Plane!=1) && (Header.Plane!=2) && (Header.Plane!=4) && (Header.Plane!=8) && (Header.Plane!=3) )
        || (Header.X_max<Header.X_min)
        || (Header.Y_max<Header.Y_min)
        || (Header.Bytes_per_plane_line&1) )
        File_error=1;
    }
    else
      File_error=1;

    close(Fichier);
  }
}


// -- Lire un fichier au format PCX -----------------------------------------

  // -- Afficher une ligne PCX cod�e sur 1 seul plan avec moins de 256 c. --
  void Draw_PCX_line(short Pos_Y, short real_line_size, byte Depth)
  {
    short Pos_X;
    byte  color;
    byte  reduction=8/Depth;
    byte  Masque=(1<<Depth)-1;
    byte  reduction_minus_one=reduction-1;

    for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
    {
      color=(LBM_buffer[Pos_X/reduction]>>((reduction_minus_one-(Pos_X%reduction))*Depth)) & Masque;
      Pixel_load_function(Pos_X,Pos_Y,color);
    }
  }

void Load_PCX(void)
{
  char filename[256];
  int  Fichier;
  struct PCX_header
  {
    byte Manufacturer;       // |_ Il font chier ces cons! Ils auraient pu
    byte Version;            // |  mettre une vraie signature!
    byte Compression;        // L'image est-elle compress�e?
    byte Depth;              // Nombre de bits pour coder un pixel (inutile puisqu'on se sert de Plane)
    word X_min;              // |_ Coin haut-gauche   |
    word Y_min;              // |  de l'image         |_ (Cr�tin!)
    word X_max;              // |_ Coin bas-droit     |
    word Y_max;              // |  de l'image         |
    word X_dpi;              // |_ Densit� de |_ (Presque inutile parce que
    word Y_dpi;              // |  l'image    |  aucun moniteur n'est pareil!)
    byte Palette_16c[48];    // Palette 16 coul (inutile pour 256c) (d�bile!)
    byte Reserved;           // Ca me plait �a aussi!
    byte Plane;              // 4 => 16c , 1 => 256c , ...
    word Bytes_per_plane_line;// Doit toujours �tre pair
    word Palette_info;       // 1 => color , 2 => Gris (ignor� � partir de la version 4)
    word Screen_X;           // |_ Dimensions de
    word Screen_Y;           // |  l'�cran d'origine
    byte Filler[54];         // Ca... J'adore!
  } Header; // Je hais ce header!
  short line_size;
  short real_line_size; // Largeur de l'image corrig�e
  short width_read;
  short Pos_X;
  short Pos_Y;
  byte  Octet1;
  byte  Octet2;
  byte  index;
  dword nb_colors;
  long  file_size;
  byte  palette_CGA[9]={ 84,252,252,  252, 84,252,  252,252,252};

  long  Position;
  long  image_size;
  byte * buffer;


  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);

    if (read(Fichier,&Header,sizeof(struct PCX_header))==sizeof(struct PCX_header))
    {
      Main_image_width=Header.X_max-Header.X_min+1;
      Main_image_height=Header.Y_max-Header.Y_min+1;

      Original_screen_X=Header.Screen_X;
      Original_screen_Y=Header.Screen_Y;

      if (Header.Plane!=3)
      {
        Init_preview(Main_image_width,Main_image_height,file_size,FORMAT_PCX);
        if (File_error==0)
        {
          // On pr�pare la palette � accueillir les valeurs du fichier PCX
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));
          else
            Palette_64_to_256(Main_palette);
          nb_colors=(dword)(1<<Header.Plane)<<(Header.Depth-1);

          if (nb_colors>4)
            memcpy(Main_palette,Header.Palette_16c,48);
          else
          {
            Main_palette[1].R=0;
            Main_palette[1].V=0;
            Main_palette[1].B=0;
            Octet1=Header.Palette_16c[3]>>5;
            if (nb_colors==4)
            { // Pal. CGA "alakon" (du Turc Allahkoum qui signifie "� la con" :))
              memcpy(Main_palette+1,palette_CGA,9);
              if (!(Octet1&2))
              {
                Main_palette[1].B=84;
                Main_palette[2].B=84;
                Main_palette[3].B=84;
              }
            } // Palette monochrome (on va dire que c'est du N&B)
            else
            {
              Main_palette[1].R=252;
              Main_palette[1].V=252;
              Main_palette[1].B=252;
            }
          }

          //   On se positionne � la fin du fichier - 769 octets pour voir s'il y
          // a une palette.
          if ( (Header.Depth==8) && (Header.Version>=5) && (file_size>sizeof(T_Palette)) )
          {
            lseek(Fichier,file_size-(sizeof(T_Palette)+1),SEEK_SET);
            // On regarde s'il y a une palette apr�s les donn�es de l'image
            if (read(Fichier,&Octet1,1)==1)
              if (Octet1==12) // Lire la palette si c'est une image en 256 couleurs
              {
                // On lit la palette 256c que ces cr�tins ont foutue � la fin du fichier
                if (read(Fichier,Main_palette,sizeof(T_Palette))!=sizeof(T_Palette))
                  File_error=2;
              }
          }
          Palette_256_to_64(Main_palette);
          Set_palette(Main_palette);
          Remap_fileselector();

          //   Maintenant qu'on a lu la palette que ces cr�tins sont all�s foutre
          // � la fin, on retourne juste apr�s le header pour lire l'image.
          lseek(Fichier,sizeof(struct PCX_header),SEEK_SET);

          if (!File_error)
          {
            line_size=Header.Bytes_per_plane_line*Header.Plane;
            real_line_size=(short)Header.Bytes_per_plane_line<<3;
            //   On se sert de donn�es LBM car le dessin de ligne en moins de 256
            // couleurs se fait comme avec la structure ILBM.
            Image_HAM=0;
            HBPm1=Header.Plane-1;
            LBM_buffer=(byte *)malloc(line_size);

            // Chargement de l'image
            if (Header.Compression)  // Image compress�e
            {
              Init_lecture();

              image_size=(long)Header.Bytes_per_plane_line*Main_image_height;

              if (Header.Depth==8) // 256 couleurs (1 plan)
              {
                for (Position=0; ((Position<image_size) && (!File_error));)
                {
                  // Lecture et d�compression de la ligne
                  Octet1=Read_one_byte(Fichier);
                  if (!File_error)
                  {
                    if ((Octet1&0xC0)==0xC0)
                    {
                      Octet1-=0xC0;               // facteur de r�p�tition
                      Octet2=Read_one_byte(Fichier); // octet � r�p�ter
                      if (!File_error)
                      {
                        for (index=0; index<Octet1; index++,Position++)
                          if (Position<image_size)
                            Pixel_load_function(Position%line_size,
                                                Position/line_size,
                                                Octet2);
                          else
                            File_error=2;
                      }
                    }
                    else
                    {
                      Pixel_load_function(Position%line_size,
                                          Position/line_size,
                                          Octet1);
                      Position++;
                    }
                  }
                }
              }
              else                 // couleurs rang�es par plans
              {
                for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
                {
                  for (Pos_X=0; ((Pos_X<line_size) && (!File_error)); )
                  {
                    Octet1=Read_one_byte(Fichier);
                    if (!File_error)
                    {
                      if ((Octet1&0xC0)==0xC0)
                      {
                        Octet1-=0xC0;               // facteur de r�p�tition
                        Octet2=Read_one_byte(Fichier); // octet � r�p�ter
                        if (!File_error)
                        {
                          for (index=0; index<Octet1; index++)
                            if (Pos_X<line_size)
                              LBM_buffer[Pos_X++]=Octet2;
                            else
                              File_error=2;
                        }
                        else
                          Set_file_error(2);
                      }
                      else
                        LBM_buffer[Pos_X++]=Octet1;
                    }
                  }
                  // Affichage de la ligne par plan du buffer
                  if (Header.Depth==1)
                    Draw_ILBM_line(Pos_Y,real_line_size);
                  else
                    Draw_PCX_line(Pos_Y,real_line_size,Header.Depth);
                }
              }

              Close_lecture();
            }
            else                     // Image non compress�e
            {
              for (Pos_Y=0;(Pos_Y<Main_image_height) && (!File_error);Pos_Y++)
              {
                if ((width_read=read(Fichier,LBM_buffer,line_size))==line_size)
                {
                  if (Header.Plane==1)
                    for (Pos_X=0; Pos_X<Main_image_width;Pos_X++)
                      Pixel_load_function(Pos_X,Pos_Y,LBM_buffer[Pos_X]);
                  else
                  {
                    if (Header.Depth==1)
                      Draw_ILBM_line(Pos_Y,real_line_size);
                    else
                      Draw_PCX_line(Pos_Y,real_line_size,Header.Depth);
                  }
                }
                else
                  File_error=2;
              }
            }

            free(LBM_buffer);
          }
        }
      }
      else
      {
        // Image 24 bits!!!

        Init_preview(Main_image_width,Main_image_height,file_size,FORMAT_PCX | FORMAT_24B);

        if (File_error==0)
        {
          line_size=Header.Bytes_per_plane_line*3;
          buffer=(byte *)malloc(line_size);

          if (!Header.Compression)
          {
            for (Pos_Y=0;(Pos_Y<Main_image_height) && (!File_error);Pos_Y++)
            {
              if (read(Fichier,buffer,line_size)==line_size)
              {
                for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
                  Pixel_load_24b(Pos_X,Pos_Y,buffer[Pos_X+(Header.Bytes_per_plane_line*0)],buffer[Pos_X+(Header.Bytes_per_plane_line*1)],buffer[Pos_X+(Header.Bytes_per_plane_line*2)]);
              }
              else
                File_error=2;
            }
          }
          else
          {
            Init_lecture();

            for (Pos_Y=0,Position=0;(Pos_Y<Main_image_height) && (!File_error);)
            {
              // Lecture et d�compression de la ligne
              Octet1=Read_one_byte(Fichier);
              if (!File_error)
              {
                if ((Octet1 & 0xC0)==0xC0)
                {
                  Octet1-=0xC0;               // facteur de r�p�tition
                  Octet2=Read_one_byte(Fichier); // octet � r�p�ter
                  if (!File_error)
                  {
                    for (index=0; (index<Octet1) && (!File_error); index++)
                    {
                      buffer[Position++]=Octet2;
                      if (Position>=line_size)
                      {
                        for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
                          Pixel_load_24b(Pos_X,Pos_Y,buffer[Pos_X+(Header.Bytes_per_plane_line*0)],buffer[Pos_X+(Header.Bytes_per_plane_line*1)],buffer[Pos_X+(Header.Bytes_per_plane_line*2)]);
                        Pos_Y++;
                        Position=0;
                      }
                    }
                  }
                }
                else
                {
                  buffer[Position++]=Octet1;
                  if (Position>=line_size)
                  {
                    for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
                      Pixel_load_24b(Pos_X,Pos_Y,buffer[Pos_X+(Header.Bytes_per_plane_line*0)],buffer[Pos_X+(Header.Bytes_per_plane_line*1)],buffer[Pos_X+(Header.Bytes_per_plane_line*2)]);
                    Pos_Y++;
                    Position=0;
                  }
                }
              }
            }
            if (Position!=0)
              File_error=2;

            Close_lecture();
          }
          free(buffer);
        }
      }
    }
    else
      File_error=1;

    close(Fichier);
  }
  else
    File_error=1;
}


// -- Ecrire un fichier au format PCX ---------------------------------------

void Save_PCX(void)
{
  char filename[256];
  int  Fichier;
  struct PCX_header
  {
    byte Manufacturer;
    byte Version;
    byte Compression;
    byte Depth;
    word X_min;
    word Y_min;
    word X_max;
    word Y_max;
    word X_dpi;
    word Y_dpi;
    byte Palette_16c[48];
    byte Reserved;
    byte Plane;
    word Bytes_per_plane_line;
    word Palette_info;
    word Screen_X;
    word Screen_Y;
    byte Filler[54];
  } Header;
  short line_size;
  short Pos_X;
  short Pos_Y;
  byte  Compteur;
  byte  last_pixel;
  byte  pixel_read;



  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE))!=-1)
  {
    // On pr�pare la palette pour �crire les 16 premieres valeurs
    Palette_64_to_256(Main_palette);

    Header.Manufacturer=10;
    Header.Version=5;
    Header.Compression=1;
    Header.Depth=8;
    Header.X_min=0;
    Header.Y_min=0;
    Header.X_max=Main_image_width-1;
    Header.Y_max=Main_image_height-1;
    Header.X_dpi=0;
    Header.Y_dpi=0;
    memcpy(Header.Palette_16c,Main_palette,48);
    Header.Reserved=0;
    Header.Plane=1;
    Header.Bytes_per_plane_line=(Main_image_width&1)?Main_image_width+1:Main_image_width;
    Header.Palette_info=1;
    Header.Screen_X=Screen_width;
    Header.Screen_Y=Screen_height;
    memset(Header.Filler,0,54);

    if (write(Fichier,&Header,sizeof(struct PCX_header))!=-1)
    {
      line_size=Header.Bytes_per_plane_line*Header.Plane;

      Init_write_buffer();

      for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
      {
        pixel_read=Read_pixel_old(0,Pos_Y);

        // Compression et �criture de la ligne
        for (Pos_X=0; ((Pos_X<line_size) && (!File_error)); )
        {
          Pos_X++;
          last_pixel=pixel_read;
          pixel_read=Read_pixel_old(Pos_X,Pos_Y);
          Compteur=1;
          while ( (Compteur<63) && (Pos_X<line_size) && (pixel_read==last_pixel) )
          {
            Compteur++;
            Pos_X++;
            pixel_read=Read_pixel_old(Pos_X,Pos_Y);
          }

          if ( (Compteur>1) || (last_pixel>=0xC0) )
            Write_one_byte(Fichier,Compteur|0xC0);
          Write_one_byte(Fichier,last_pixel);

        }
      }

      // Ecriture de l'octet (12) indiquant que la palette arrive
      if (!File_error)
        Write_one_byte(Fichier,12);

      End_write(Fichier);

      // Ecriture de la palette
      if (!File_error)
      {
        if (write(Fichier,Main_palette,sizeof(T_Palette))==-1)
          File_error=1;
      }
    }
    else
      File_error=1;

    close(Fichier);

    if (File_error)
      remove(filename);

    // On remet la palette � son �tat normal
    Palette_256_to_64(Main_palette);
  }
  else
    File_error=1;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// CEL ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format CEL --------------------------------

void Test_CEL(void)
{
  char filename[256];
  int  Taille;
  int  Fichier;
  struct CEL_Header1
  {
    word Width;              // Largeur de l'image
    word Height;             // Hauteur de l'image
  } Header1;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image
    word Hauteur;            // Hauteur de l'image
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;

  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (read(Fichier,&Header1,sizeof(struct CEL_Header1))==sizeof(struct CEL_Header1))
    {
      //   Vu que ce header n'a pas de signature, il va falloir tester la
      // coh�rence de la dimension de l'image avec celle du fichier.
      Taille=filelength(Fichier)-sizeof(struct CEL_Header1);
      if ( (!Taille) || ( (((Header1.Width+1)>>1)*Header1.Height)!=Taille ) )
      {
        // Tentative de reconnaissance de la signature des nouveaux fichiers

        lseek(Fichier,0,SEEK_SET);
        if (read(Fichier,&Header2,sizeof(struct CEL_Header2))==sizeof(struct CEL_Header2))
        {
          if (memcmp(Header2.Signature,"KiSS",4)==0)
          {
            if (Header2.Kind!=0x20)
              File_error=1;
          }
          else
            File_error=1;
        }
        else
          File_error=1;
      }
    }
    else
      File_error=1;

    close(Fichier);
  }
  else
    File_error=1;
}


// -- Lire un fichier au format CEL -----------------------------------------

void Load_CEL(void)
{
  char filename[256];
  int  Fichier;
  struct CEL_Header1
  {
    word Width;              // Largeur de l'image
    word Height;             // Hauteur de l'image
  } Header1;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image
    word Hauteur;            // Hauteur de l'image
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;
  short Pos_X;
  short Pos_Y;
  byte  last_byte;
  long  file_size;


  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (read(Fichier,&Header1,sizeof(struct CEL_Header1))==sizeof(struct CEL_Header1))
    {
      file_size=filelength(Fichier);
      if ( (file_size>sizeof(struct CEL_Header1))
        && ( (((Header1.Width+1)>>1)*Header1.Height)==(file_size-sizeof(struct CEL_Header1)) ) )
      {
        // Chargement d'un fichier CEL sans signature (vieux fichiers)
        Main_image_width=Header1.Width;
        Main_image_height=Header1.Height;
        Original_screen_X=Main_image_width;
        Original_screen_Y=Main_image_height;
        Init_preview(Main_image_width,Main_image_height,file_size,FORMAT_CEL);
        if (File_error==0)
        {
          // Chargement de l'image
          Init_lecture();
          for (Pos_Y=0;((Pos_Y<Main_image_height) && (!File_error));Pos_Y++)
            for (Pos_X=0;((Pos_X<Main_image_width) && (!File_error));Pos_X++)
              if ((Pos_X & 1)==0)
              {
                last_byte=Read_one_byte(Fichier);
                Pixel_load_function(Pos_X,Pos_Y,(last_byte >> 4));
              }
              else
                Pixel_load_function(Pos_X,Pos_Y,(last_byte & 15));
          Close_lecture();
        }
      }
      else
      {
        // On r�essaye avec le nouveau format

        lseek(Fichier,0,SEEK_SET);
        if (read(Fichier,&Header2,sizeof(struct CEL_Header2))==sizeof(struct CEL_Header2))
        {
          // Chargement d'un fichier CEL avec signature (nouveaux fichiers)

          Main_image_width=Header2.Largeur+Header2.X_offset;
          Main_image_height=Header2.Hauteur+Header2.Y_offset;
          Original_screen_X=Main_image_width;
          Original_screen_Y=Main_image_height;
          Init_preview(Main_image_width,Main_image_height,file_size,FORMAT_CEL);
          if (File_error==0)
          {
            // Chargement de l'image
            Init_lecture();

            if (!File_error)
            {
              // Effacement du d�calage
              for (Pos_Y=0;Pos_Y<Header2.Y_offset;Pos_Y++)
                for (Pos_X=0;Pos_X<Main_image_width;Pos_X++)
                  Pixel_load_function(Pos_X,Pos_Y,0);
              for (Pos_Y=Header2.Y_offset;Pos_Y<Main_image_height;Pos_Y++)
                for (Pos_X=0;Pos_X<Header2.X_offset;Pos_X++)
                  Pixel_load_function(Pos_X,Pos_Y,0);

              switch(Header2.Nb_bits)
              {
                case 4:
                  for (Pos_Y=0;((Pos_Y<Header2.Hauteur) && (!File_error));Pos_Y++)
                    for (Pos_X=0;((Pos_X<Header2.Largeur) && (!File_error));Pos_X++)
                      if ((Pos_X & 1)==0)
                      {
                        last_byte=Read_one_byte(Fichier);
                        Pixel_load_function(Pos_X+Header2.X_offset,Pos_Y+Header2.Y_offset,(last_byte >> 4));
                      }
                      else
                        Pixel_load_function(Pos_X+Header2.X_offset,Pos_Y+Header2.Y_offset,(last_byte & 15));
                  break;

                case 8:
                  for (Pos_Y=0;((Pos_Y<Header2.Hauteur) && (!File_error));Pos_Y++)
                    for (Pos_X=0;((Pos_X<Header2.Largeur) && (!File_error));Pos_X++)
                      Pixel_load_function(Pos_X+Header2.X_offset,Pos_Y+Header2.Y_offset,Read_one_byte(Fichier));
                  break;

                default:
                  File_error=1;
              }
            }
            Close_lecture();
          }
        }
        else
          File_error=1;
      }
      close(Fichier);
    }
    else
      File_error=1;
  }
  else
    File_error=1;
}


// -- Ecrire un fichier au format CEL ---------------------------------------

void Save_CEL(void)
{
  char filename[256];
  int  Fichier;
  struct CEL_Header1
  {
    word Width;              // Largeur de l'image
    word Height;             // Hauteur de l'image
  } Header1;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image
    word Hauteur;            // Hauteur de l'image
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;
  short Pos_X;
  short Pos_Y;
  byte  last_byte;
  dword Utilisation[256]; // Table d'utilisation de couleurs


  // On commence par compter l'utilisation de chaque couleurs
  Count_used_colors(Utilisation);

  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE))!=-1)
  {
    // On regarde si des couleurs >16 sont utilis�es dans l'image
    for (Pos_X=16;((Pos_X<256) && (!Utilisation[Pos_X]));Pos_X++);

    if (Pos_X==256)
    {
      // Cas d'une image 16 couleurs (�criture � l'ancien format)

      Header1.Width =Main_image_width;
      Header1.Height=Main_image_height;

      if (write(Fichier,&Header1,sizeof(struct CEL_Header1))!=-1)
      {
        // Sauvegarde de l'image
        Init_write_buffer();
        for (Pos_Y=0;((Pos_Y<Main_image_height) && (!File_error));Pos_Y++)
        {
          for (Pos_X=0;((Pos_X<Main_image_width) && (!File_error));Pos_X++)
            if ((Pos_X & 1)==0)
              last_byte=(Read_pixel_old(Pos_X,Pos_Y) << 4);
            else
            {
              last_byte=last_byte | (Read_pixel_old(Pos_X,Pos_Y) & 15);
              Write_one_byte(Fichier,last_byte);
            }

          if ((Pos_X & 1)==1)
            Write_one_byte(Fichier,last_byte);
        }
        End_write(Fichier);
      }
      else
        File_error=1;
      close(Fichier);
    }
    else
    {
      // Cas d'une image 256 couleurs (�criture au nouveau format)

      // Recherche du d�calage
      for (Pos_Y=0;Pos_Y<Main_image_height;Pos_Y++)
      {
        for (Pos_X=0;Pos_X<Main_image_width;Pos_X++)
          if (Read_pixel_old(Pos_X,Pos_Y)!=0)
            break;
        if (Read_pixel_old(Pos_X,Pos_Y)!=0)
          break;
      }
      Header2.Y_offset=Pos_Y;
      for (Pos_X=0;Pos_X<Main_image_width;Pos_X++)
      {
        for (Pos_Y=0;Pos_Y<Main_image_height;Pos_Y++)
          if (Read_pixel_old(Pos_X,Pos_Y)!=0)
            break;
        if (Read_pixel_old(Pos_X,Pos_Y)!=0)
          break;
      }
      Header2.X_offset=Pos_X;

      memcpy(Header2.Signature,"KiSS",4); // Initialisation de la signature
      Header2.Kind=0x20;              // Initialisation du type (BitMaP)
      Header2.Nb_bits=8;               // Initialisation du nombre de bits
      Header2.Filler1=0;              // Initialisation du filler 1 (???)
      Header2.Largeur=Main_image_width-Header2.X_offset; // Initialisation de la largeur
      Header2.Hauteur=Main_image_height-Header2.Y_offset; // Initialisation de la hauteur
      for (Pos_X=0;Pos_X<16;Pos_X++)  // Initialisation du filler 2 (???)
        Header2.Filler2[Pos_X]=0;

      if (write(Fichier,&Header2,sizeof(struct CEL_Header2))!=-1)
      {
        // Sauvegarde de l'image
        Init_write_buffer();
        for (Pos_Y=0;((Pos_Y<Header2.Hauteur) && (!File_error));Pos_Y++)
          for (Pos_X=0;((Pos_X<Header2.Largeur) && (!File_error));Pos_X++)
            Write_one_byte(Fichier,Read_pixel_old(Pos_X+Header2.X_offset,Pos_Y+Header2.Y_offset));
        End_write(Fichier);
      }
      else
        File_error=1;
      close(Fichier);
    }

    if (File_error)
      remove(filename);
  }
  else
    File_error=1;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// KCF ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format KCF --------------------------------

void Test_KCF(void)
{
  char filename[256];
  int  Fichier;
  struct KCF_Header
  {
    struct
    {
      struct
      {
        byte Octet1;
        byte Octet2;
      } color[16];
    } Palette[10];
  } buffer;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image
    word Hauteur;            // Hauteur de l'image
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;
  int pal_index;
  int color_index;

  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    if (filelength(Fichier)==sizeof(struct KCF_Header))
    {
      read(Fichier,&buffer,sizeof(struct KCF_Header));
      // On v�rifie une propri�t� de la structure de palette:
      for (pal_index=0;pal_index<10;pal_index++)
        for (color_index=0;color_index<16;color_index++)
          if ((buffer.Palette[pal_index].color[color_index].Octet2>>4)!=0)
            File_error=1;
    }
    else
    {
      if (read(Fichier,&Header2,sizeof(struct CEL_Header2))==sizeof(struct CEL_Header2))
      {
        if (memcmp(Header2.Signature,"KiSS",4)==0)
        {
          if (Header2.Kind!=0x10)
            File_error=1;
        }
        else
          File_error=1;
      }
      else
        File_error=1;
    }
    close(Fichier);
  }
  else
    File_error=1;
}


// -- Lire un fichier au format KCF -----------------------------------------

void Load_KCF(void)
{
  char filename[256];
  int  Fichier;
  struct KCF_Header
  {
    struct
    {
      struct
      {
        byte Octet1;
        byte Octet2;
      } color[16];
    } Palette[10];
  } buffer;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image ou nb de couleurs d�finies
    word Hauteur;            // Hauteur de l'image ou nb de palettes d�finies
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;
  byte Octet[3];
  int pal_index;
  int color_index;
  int index;
  long  file_size;


  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);
    if (file_size==sizeof(struct KCF_Header))
    {
      // Fichier KCF � l'ancien format

      if (read(Fichier,&buffer,sizeof(struct KCF_Header))==sizeof(struct KCF_Header))
      {
        // Init_preview(???); // Pas possible... pas d'image...

        if (Config.Clear_palette)
          memset(Main_palette,0,sizeof(T_Palette));

        // Chargement de la palette
        for (pal_index=0;pal_index<10;pal_index++)
          for (color_index=0;color_index<16;color_index++)
          {
            index=16+(pal_index*16)+color_index;
            Main_palette[index].R=((buffer.Palette[pal_index].color[color_index].Octet1 >> 4) << 2);
            Main_palette[index].B=((buffer.Palette[pal_index].color[color_index].Octet1 & 15) << 2);
            Main_palette[index].V=((buffer.Palette[pal_index].color[color_index].Octet2 & 15) << 2);
          }

        for (index=0;index<16;index++)
        {
          Main_palette[index].R=Main_palette[index+16].R;
          Main_palette[index].V=Main_palette[index+16].V;
          Main_palette[index].B=Main_palette[index+16].B;
        }

        Set_palette(Main_palette);
        Remap_fileselector();
      }
      else
        File_error=1;
    }
    else
    {
      // Fichier KCF au nouveau format

      if (read(Fichier,&Header2,sizeof(struct CEL_Header2))==sizeof(struct CEL_Header2))
      {
        // Init_preview(???); // Pas possible... pas d'image...

        index=(Header2.Nb_bits==12)?16:0;
        for (pal_index=0;pal_index<Header2.Hauteur;pal_index++)
        {
           // Pour chaque palette

           for (color_index=0;color_index<Header2.Largeur;color_index++)
           {
             // Pour chaque couleur

             switch(Header2.Nb_bits)
             {
               case 12: // RRRR BBBB | 0000 VVVV
                 read(Fichier,Octet,2);
                 Main_palette[index].R=(Octet[0] >> 4) << 2;
                 Main_palette[index].B=(Octet[0] & 15) << 2;
                 Main_palette[index].V=(Octet[1] & 15) << 2;
                 break;

               case 24: // RRRR RRRR | VVVV VVVV | BBBB BBBB
                 read(Fichier,Octet,3);
                 Main_palette[index].R=Octet[0]>>2;
                 Main_palette[index].V=Octet[1]>>2;
                 Main_palette[index].B=Octet[2]>>2;
             }

             index++;
           }
        }

        if (Header2.Nb_bits==12)
          for (index=0;index<16;index++)
          {
            Main_palette[index].R=Main_palette[index+16].R;
            Main_palette[index].V=Main_palette[index+16].V;
            Main_palette[index].B=Main_palette[index+16].B;
          }

        Set_palette(Main_palette);
        Remap_fileselector();
      }
      else
        File_error=1;
    }
    close(Fichier);
  }
  else
    File_error=1;

  if (!File_error) Draw_palette_preview();
}


// -- Ecrire un fichier au format KCF ---------------------------------------

void Save_KCF(void)
{
  char filename[256];
  int  Fichier;
  struct KCF_Header
  {
    struct
    {
      struct
      {
        byte Octet1;
        byte Octet2;
      } color[16];
    } Palette[10];
  } buffer;
  struct CEL_Header2
  {
    byte Signature[4];           // Signature du format
    byte Kind;               // Type de fichier ($10=PALette $20=BitMaP)
    byte Nb_bits;             // Nombre de bits
    word Filler1;            // ???
    word Largeur;            // Largeur de l'image ou nb de couleurs d�finies
    word Hauteur;            // Hauteur de l'image ou nb de palettes d�finies
    word X_offset;         // Offset en X de l'image
    word Y_offset;         // Offset en Y de l'image
    byte Filler2[16];        // ???
  } Header2;
  byte Octet[3];
  int pal_index;
  int color_index;
  int index;
  dword Utilisation[256]; // Table d'utilisation de couleurs

  // On commence par compter l'utilisation de chaque couleurs
  Count_used_colors(Utilisation);

  File_error=0;
  filename_complet(filename,0);
  if ((Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE))!=-1)
  {
    // Sauvegarde de la palette

    // On regarde si des couleurs >16 sont utilis�es dans l'image
    for (index=16;((index<256) && (!Utilisation[index]));index++);

    if (index==256)
    {
      // Cas d'une image 16 couleurs (�criture � l'ancien format)

      for (pal_index=0;pal_index<10;pal_index++)
        for (color_index=0;color_index<16;color_index++)
        {
          index=16+(pal_index*16)+color_index;
          buffer.Palette[pal_index].color[color_index].Octet1=((Main_palette[index].R>>2)<<4) | (Main_palette[index].B>>2);
          buffer.Palette[pal_index].color[color_index].Octet2=Main_palette[index].V>>2;
        }

      if (write(Fichier,&buffer,sizeof(struct KCF_Header))!=sizeof(struct KCF_Header))
        File_error=1;
    }
    else
    {
      // Cas d'une image 256 couleurs (�criture au nouveau format)

      memcpy(Header2.Signature,"KiSS",4); // Initialisation de la signature
      Header2.Kind=0x10;              // Initialisation du type (PALette)
      Header2.Nb_bits=24;              // Initialisation du nombre de bits
      Header2.Filler1=0;              // Initialisation du filler 1 (???)
      Header2.Largeur=256;            // Initialisation du nombre de couleurs
      Header2.Hauteur=1;              // Initialisation du nombre de palettes
      Header2.X_offset=0;           // Initialisation du d�calage X
      Header2.Y_offset=0;           // Initialisation du d�calage Y
      for (index=0;index<16;index++) // Initialisation du filler 2 (???)
        Header2.Filler2[index]=0;

      if (write(Fichier,&Header2,sizeof(struct CEL_Header2))!=sizeof(struct CEL_Header2))
        File_error=1;

      for (index=0;(index<256) && (!File_error);index++)
      {
        Octet[0]=Main_palette[index].R<<2;
        Octet[1]=Main_palette[index].V<<2;
        Octet[2]=Main_palette[index].B<<2;
        if (write(Fichier,Octet,3)!=3)
          File_error=1;
      }
    }

    close(Fichier);

    if (File_error)
      remove(filename);
  }
  else
    File_error=1;
}





/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SCx ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


// -- Tester si un fichier est au format SCx --------------------------------
void Test_SCx(void)
{
  int  Handle;              // Handle du fichier
  char filename[256]; // Nom complet du fichier
  byte Signature[3];
  struct Header
  {
    byte Filler1[4];
    word Largeur;
    word Hauteur;
    byte Filler2;
    byte Planes;
  } SCx_Header;


  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Handle=open(filename,O_RDONLY|O_BINARY);
  if (Handle!=-1)
  {
    // Lecture et v�rification de la signature
    if ((read(Handle,&SCx_Header,sizeof(struct Header)))==sizeof(struct Header))
    {
      if ( (!memcmp(SCx_Header.Filler1,"RIX",3))
        && SCx_Header.Largeur && SCx_Header.Hauteur)
      File_error=0;
    }
    // Fermeture du fichier
    close(Handle);
  }
}


// -- Lire un fichier au format SCx -----------------------------------------
void Load_SCx(void)
{
  char filename[256]; // Nom complet du fichier
  int  Fichier;
  word Pos_X,Pos_Y;
  long Taille,real_size;
  long file_size;
  struct Header
  {
    byte Filler1[4];
    word Largeur;
    word Hauteur;
    byte Filler2;
    byte Planes;
  } SCx_Header;
  T_Palette SCx_Palette;


  filename_complet(filename,0);

  File_error=0;

  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    file_size=filelength(Fichier);

    if ((read(Fichier,&SCx_Header,sizeof(struct Header)))==sizeof(struct Header))
    {
      Init_preview(SCx_Header.Largeur,SCx_Header.Hauteur,file_size,FORMAT_SCx);
      if (File_error==0)
      {
        if (!SCx_Header.Planes)
          Taille=sizeof(T_Palette);
        else
          Taille=sizeof(struct T_Components)*(1<<SCx_Header.Planes);

        if (read(Fichier,SCx_Palette,Taille)==Taille)
        {
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));

          memcpy(Main_palette,SCx_Palette,Taille);
          Set_palette(Main_palette);
          Remap_fileselector();

          Main_image_width=SCx_Header.Largeur;
          Main_image_height=SCx_Header.Hauteur;

          if (!SCx_Header.Planes)
          { // 256 couleurs (raw)
            LBM_buffer=(byte *)malloc(Main_image_width);

            for (Pos_Y=0;(Pos_Y<Main_image_height) && (!File_error);Pos_Y++)
            {
              if (read(Fichier,LBM_buffer,Main_image_width)==Main_image_width)
                for (Pos_X=0; Pos_X<Main_image_width;Pos_X++)
                  Pixel_load_function(Pos_X,Pos_Y,LBM_buffer[Pos_X]);
              else
                File_error=2;
            }
          }
          else
          { // moins de 256 couleurs (planar)
            Taille=((Main_image_width+7)>>3)*SCx_Header.Planes;
            real_size=(Taille/SCx_Header.Planes)<<3;
            LBM_buffer=(byte *)malloc(Taille);
            HBPm1=SCx_Header.Planes-1;
            Image_HAM=0;

            for (Pos_Y=0;(Pos_Y<Main_image_height) && (!File_error);Pos_Y++)
            {
              if (read(Fichier,LBM_buffer,Taille)==Taille)
                Draw_ILBM_line(Pos_Y,real_size);
              else
                File_error=2;
            }
          }
          free(LBM_buffer);
        }
        else
          File_error=1;
      }
    }
    else
      File_error=1;

    close(Fichier);
  }
  else
    File_error=1;
}

// -- Sauver un fichier au format SCx ---------------------------------------
void Save_SCx(void)
{
  char  filename[256]; // Nom complet du fichier
  int   Fichier;
  short Pos_X,Pos_Y;
  struct Header
  {
    byte Filler1[4];
    word Largeur;
    word Hauteur;
    word Filler2;
    T_Palette Palette;
  } SCx_Header;

  filename_complet(filename,1);

  File_error=0;

  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    memcpy(SCx_Header.Filler1,"RIX3",4);
    SCx_Header.Largeur=Main_image_width;
    SCx_Header.Hauteur=Main_image_height;
    SCx_Header.Filler2=0x00AF;
    memcpy(SCx_Header.Palette,Main_palette,sizeof(T_Palette));

    if (write(Fichier,&SCx_Header,sizeof(struct Header))!=-1)
    {
      Init_write_buffer();

      for (Pos_Y=0; ((Pos_Y<Main_image_height) && (!File_error)); Pos_Y++)
        for (Pos_X=0; Pos_X<Main_image_width; Pos_X++)
          Write_one_byte(Fichier,Read_pixel_old(Pos_X,Pos_Y));

      End_write(Fichier);
      close(Fichier);

      if (File_error)
        remove(filename);
    }
    else // Error d'�criture (disque plein ou prot�g�)
    {
      close(Fichier);
      remove(filename);
      File_error=1;
    }
  }
  else
  {
    close(Fichier);
    remove(filename);
    File_error=1;
  }
}





/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PI1 ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


//// DECODAGE d'une partie d'IMAGE ////

void PI1_8b_to_16p(byte * src,byte * dest)
{
  int  i;           // index du pixel � calculer
  word masque;      // Masque de decodage
  word w0,w1,w2,w3; // Les 4 words bien ordonn�s de la source

  masque=0x8000;
  w0=(((word)src[0])<<8) | src[1];
  w1=(((word)src[2])<<8) | src[3];
  w2=(((word)src[4])<<8) | src[5];
  w3=(((word)src[6])<<8) | src[7];
  for (i=0;i<16;i++)
  {
    // Pour d�coder le pixel n�i, il faut traiter les 4 words sur leur bit
    // correspondant � celui du masque

    dest[i]=((w0 & masque)?0x01:0x00) |
           ((w1 & masque)?0x02:0x00) |
           ((w2 & masque)?0x04:0x00) |
           ((w3 & masque)?0x08:0x00);
    masque>>=1;
  }
}

//// CODAGE d'une partie d'IMAGE ////

void PI1_16p_to_8b(byte * src,byte * dest)
{
  int  i;           // index du pixel � calculer
  word masque;      // Masque de codage
  word w0,w1,w2,w3; // Les 4 words bien ordonn�s de la destination

  masque=0x8000;
  w0=w1=w2=w3=0;
  for (i=0;i<16;i++)
  {
    // Pour coder le pixel n�i, il faut modifier les 4 words sur leur bit
    // correspondant � celui du masque

    w0|=(src[i] & 0x01)?masque:0x00;
    w1|=(src[i] & 0x02)?masque:0x00;
    w2|=(src[i] & 0x04)?masque:0x00;
    w3|=(src[i] & 0x08)?masque:0x00;
    masque>>=1;
  }
  dest[0]=w0 >> 8;
  dest[1]=w0 & 0x00FF;
  dest[2]=w1 >> 8;
  dest[3]=w1 & 0x00FF;
  dest[4]=w2 >> 8;
  dest[5]=w2 & 0x00FF;
  dest[6]=w3 >> 8;
  dest[7]=w3 & 0x00FF;
}

//// DECODAGE de la PALETTE ////

void PI1_decode_palette(byte * src,byte * Pal)
{
  int i;  // Num�ro de la couleur trait�e
  int ip; // index dans la palette
  word w; // Word contenant le code

  // Sch�ma d'un word =
  //
  //    Low        High
  // VVVV RRRR | 0000 BBBB
  // 0321 0321 |      0321

  ip=0;
  for (i=0;i<16;i++)
  {
    w=((word)src[(i*2)+1]<<8) | src[(i*2)+0];

    // Traitement des couleurs rouge, verte et bleue:
    Pal[ip++]=(((w & 0x0007) <<  1) | ((w & 0x0008) >>  3)) << 2;
    Pal[ip++]=(((w & 0x7000) >> 11) | ((w & 0x8000) >> 15)) << 2;
    Pal[ip++]=(((w & 0x0700) >>  7) | ((w & 0x0800) >> 11)) << 2;
  }
}

//// CODAGE de la PALETTE ////

void PI1_code_palette(byte * Pal,byte * dest)
{
  int i;  // Num�ro de la couleur trait�e
  int ip; // index dans la palette
  word w; // Word contenant le code

  // Sch�ma d'un word =
  //
  //    Low        High
  // VVVV RRRR | 0000 BBBB
  // 0321 0321 |      0321

  ip=0;
  for (i=0;i<16;i++)
  {
    // Traitement des couleurs rouge, verte et bleue:
    w =(((word)Pal[ip] & 0x38) >> 3) | (((word)Pal[ip] & 0x04) <<  1); ip++;
    w|=(((word)Pal[ip] & 0x38) << 9) | (((word)Pal[ip] & 0x04) << 13); ip++;
    w|=(((word)Pal[ip] & 0x38) << 5) | (((word)Pal[ip] & 0x04) <<  9); ip++;

    dest[(i*2)+0]=w & 0x00FF;
    dest[(i*2)+1]=(w>>8);
  }
}


// -- Tester si un fichier est au format PI1 --------------------------------
void Test_PI1(void)
{
  int  Handle;              // Handle du fichier
  char filename[256]; // Nom complet du fichier
  int  Taille;              // Taille du fichier
  word resolution;                 // R�solution de l'image


  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Handle=open(filename,O_RDONLY|O_BINARY);
  if (Handle!=-1)
  {
    // V�rification de la taille
    Taille=filelength(Handle);
    if ((Taille==32034) || (Taille==32066))
    {
      // Lecture et v�rification de la r�solution
      if ((read(Handle,&resolution,2))==2)
      {
        if (resolution==0x0000)
          File_error=0;
      }
    }
    // Fermeture du fichier
    close(Handle);
  }
}


// -- Lire un fichier au format PI1 -----------------------------------------
void Load_PI1(void)
{
  char filename[256]; // Nom complet du fichier
  int  Fichier;
  word Pos_X,Pos_Y;
  byte * buffer;
  byte * ptr;
  byte pixels[320];

  filename_complet(filename,0);

  File_error=0;
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    // allocation d'un buffer m�moire
    buffer=(byte *)malloc(32034);
    if (buffer!=NULL)
    {
      // Lecture du fichier dans le buffer
      if (read(Fichier,buffer,32034)==32034)
      {
        // Initialisation de la preview
        Init_preview(320,200,filelength(Fichier),FORMAT_PI1);
        if (File_error==0)
        {
          // Initialisation de la palette
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));
          PI1_decode_palette(buffer+2,(byte *)Main_palette);
          Set_palette(Main_palette);
          Remap_fileselector();

          Main_image_width=320;
          Main_image_height=200;

          // Chargement/d�compression de l'image
          ptr=buffer+34;
          for (Pos_Y=0;Pos_Y<200;Pos_Y++)
          {
            for (Pos_X=0;Pos_X<(320>>4);Pos_X++)
            {
              PI1_8b_to_16p(ptr,pixels+(Pos_X<<4));
              ptr+=8;
            }
            for (Pos_X=0;Pos_X<320;Pos_X++)
              Pixel_load_function(Pos_X,Pos_Y,pixels[Pos_X]);
          }
        }
      }
      else
        File_error=1;
      free(buffer);
    }
    else
      File_error=1;
    close(Fichier);
  }
  else
    File_error=1;
}


// -- Sauver un fichier au format PI1 ---------------------------------------
void Save_PI1(void)
{
  char  filename[256]; // Nom complet du fichier
  int   Fichier;
  short Pos_X,Pos_Y;
  byte * buffer;
  byte * ptr;
  byte pixels[320];

  filename_complet(filename,0);

  File_error=0;
  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    // allocation d'un buffer m�moire
    buffer=(byte *)malloc(32034);
    // Codage de la r�solution
    buffer[0]=0x00;
    buffer[1]=0x00;
    // Codage de la palette
    PI1_code_palette((byte *)Main_palette,buffer+2);
    // Codage de l'image
    ptr=buffer+34;
    for (Pos_Y=0;Pos_Y<200;Pos_Y++)
    {
      // Codage de la ligne
      memset(pixels,0,320);
      if (Pos_Y<Main_image_height)
      {
        for (Pos_X=0;(Pos_X<320) && (Pos_X<Main_image_width);Pos_X++)
          pixels[Pos_X]=Read_pixel_old(Pos_X,Pos_Y);
      }

      for (Pos_X=0;Pos_X<(320>>4);Pos_X++)
      {
        PI1_16p_to_8b(pixels+(Pos_X<<4),ptr);
        ptr+=8;
      }
    }

    if (write(Fichier,buffer,32034)==32034)
    {
      close(Fichier);
    }
    else // Error d'�criture (disque plein ou prot�g�)
    {
      close(Fichier);
      remove(filename);
      File_error=1;
    }
    // Lib�ration du buffer m�moire
    free(buffer);
  }
  else
  {
    close(Fichier);
    remove(filename);
    File_error=1;
  }
}





/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// PC1 ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


//// DECOMPRESSION d'un buffer selon la m�thode PACKBITS ////

void PC1_uncompress_packbits(byte * src,byte * dest)
{
  int is,id; // Les indices de parcour des buffers
  int n;     // Octet de contr�le

  for (is=id=0;id<32000;)
  {
    n=src[is++];

    if (n & 0x80)
    {
      // Recopier src[is] -n+1 fois
      n=257-n;
      for (;(n>0) && (id<32000);n--)
        dest[id++]=src[is];
      is++;
    }
    else
    {
      // Recopier n+1 octets litt�ralement
      n=n+1;
      for (;(n>0) && (id<32000);n--)
        dest[id++]=src[is++];
    }

    // Contr�le des erreurs
    if (n>0)
      File_error=1;
  }
}

//// COMPRESSION d'un buffer selon la m�thode PACKBITS ////

void PC1_compress_packbits(byte * src,byte * dest,int source_size,int * dest_size)
{
  int is; // index dans la source
  int id; // index dans la destination
  int ir; // index de   la r�p�tition
  int n;  // Taille des s�quences
  int repet; // "Il y a r�p�tition"

  for (is=id=0;is<source_size;)
  {
    // On recherche le 1er endroit o� il y a r�p�tition d'au moins 3 valeurs
    // identiques

    repet=0;
    for (ir=is;ir<source_size-2;ir++)
    {
      if ((src[ir]==src[ir+1]) && (src[ir+1]==src[ir+2]))
      {
        repet=1;
        break;
      }
      if ((ir-is)+1==40)
        break;
    }

    // On code la partie sans r�p�titions
    if (ir!=is)
    {
      n=(ir-is)+1;
      dest[id++]=n-1;
      for (;n>0;n--)
        dest[id++]=src[is++];
    }

    // On code la partie sans r�p�titions
    if (repet)
    {
      // On compte la quantit� de fois qu'il faut r�p�ter la valeur
      for (ir+=3;ir<source_size;ir++)
      {
        if (src[ir]!=src[is])
          break;
        if ((ir-is)+1==40)
          break;
      }
      n=(ir-is);
      dest[id++]=257-n;
      dest[id++]=src[is];
      is=ir;
    }
  }

  // On renseigne la taille du buffer compress�
  *dest_size=id;
}

//// DECODAGE d'une partie d'IMAGE ////

// Transformation de 4 plans de bits en 1 ligne de pixels

void PC1_4bp_to_1line(byte * src0,byte * src1,byte * src2,byte * src3,byte * dest)
{
  int  i,j;         // Compteurs
  int  ip;          // index du pixel � calculer
  byte masque;      // Masque de decodage
  byte b0,b1,b2,b3; // Les 4 octets des plans bits sources

  ip=0;
  // Pour chacun des 40 octets des plans de bits
  for (i=0;i<40;i++)
  {
    b0=src0[i];
    b1=src1[i];
    b2=src2[i];
    b3=src3[i];
    // Pour chacun des 8 bits des octets
    masque=0x80;
    for (j=0;j<8;j++)
    {
      dest[ip++]=((b0 & masque)?0x01:0x00) |
                ((b1 & masque)?0x02:0x00) |
                ((b2 & masque)?0x04:0x00) |
                ((b3 & masque)?0x08:0x00);
      masque>>=1;
    }
  }
}

//// CODAGE d'une partie d'IMAGE ////

// Transformation d'1 ligne de pixels en 4 plans de bits

void PC1_1line_to_4bp(byte * src,byte * dst0,byte * dst1,byte * dst2,byte * dst3)
{
  int  i,j;         // Compteurs
  int  ip;          // index du pixel � calculer
  byte masque;      // Masque de decodage
  byte b0,b1,b2,b3; // Les 4 octets des plans bits sources

  ip=0;
  // Pour chacun des 40 octets des plans de bits
  for (i=0;i<40;i++)
  {
    // Pour chacun des 8 bits des octets
    masque=0x80;
    b0=b1=b2=b3=0;
    for (j=0;j<8;j++)
    {
      b0|=(src[ip] & 0x01)?masque:0x00;
      b1|=(src[ip] & 0x02)?masque:0x00;
      b2|=(src[ip] & 0x04)?masque:0x00;
      b3|=(src[ip] & 0x08)?masque:0x00;
      ip++;
      masque>>=1;
    }
    dst0[i]=b0;
    dst1[i]=b1;
    dst2[i]=b2;
    dst3[i]=b3;
  }
}


// -- Tester si un fichier est au format PC1 --------------------------------
void Test_PC1(void)
{
  int  Handle;              // Handle du fichier
  char filename[256]; // Nom complet du fichier
  int  Taille;              // Taille du fichier
  word resolution;                 // R�solution de l'image


  filename_complet(filename,0);

  File_error=1;

  // Ouverture du fichier
  Handle=open(filename,O_RDONLY|O_BINARY);
  if (Handle!=-1)
  {
    // V�rification de la taille
    Taille=filelength(Handle);
    if ((Taille<=32066))
    {
      // Lecture et v�rification de la r�solution
      if ((read(Handle,&resolution,2))==2)
      {
        if (resolution==0x0080)
          File_error=0;
      }
    }
    // Fermeture du fichier
    close(Handle);
  }
}


// -- Lire un fichier au format PC1 -----------------------------------------
void Load_PC1(void)
{
  char filename[256]; // Nom complet du fichier
  int  Fichier;
  int  Taille;
  word Pos_X,Pos_Y;
  byte * buffercomp;
  byte * bufferdecomp;
  byte * ptr;
  byte pixels[320];

  filename_complet(filename,0);

  File_error=0;
  if ((Fichier=open(filename,O_RDONLY|O_BINARY))!=-1)
  {
    Taille=filelength(Fichier);
    // allocation des buffers m�moire
    buffercomp=(byte *)malloc(Taille);
    bufferdecomp=(byte *)malloc(32000);
    if ( (buffercomp!=NULL) && (bufferdecomp!=NULL) )
    {
      // Lecture du fichier dans le buffer
      if (read(Fichier,buffercomp,Taille)==Taille)
      {
        // Initialisation de la preview
        Init_preview(320,200,filelength(Fichier),FORMAT_PC1);
        if (File_error==0)
        {
          // Initialisation de la palette
          if (Config.Clear_palette)
            memset(Main_palette,0,sizeof(T_Palette));
          PI1_decode_palette(buffercomp+2,(byte *)Main_palette);
          Set_palette(Main_palette);
          Remap_fileselector();

          Main_image_width=320;
          Main_image_height=200;

          // D�compression du buffer
          PC1_uncompress_packbits(buffercomp+34,bufferdecomp);

          // D�codage de l'image
          ptr=bufferdecomp;
          for (Pos_Y=0;Pos_Y<200;Pos_Y++)
          {
            // D�codage de la scanline
            PC1_4bp_to_1line(ptr,ptr+40,ptr+80,ptr+120,pixels);
            ptr+=160;
            // Chargement de la ligne
            for (Pos_X=0;Pos_X<320;Pos_X++)
              Pixel_load_function(Pos_X,Pos_Y,pixels[Pos_X]);
          }
        }
      }
      else
        File_error=1;
      free(bufferdecomp);
      free(buffercomp);
    }
    else
    {
      File_error=1;
      if (bufferdecomp) free(bufferdecomp);
      if (buffercomp)   free(buffercomp);
    }
    close(Fichier);
  }
  else
    File_error=1;
}


// -- Sauver un fichier au format PC1 ---------------------------------------
void Save_PC1(void)
{
  char  filename[256]; // Nom complet du fichier
  int   Fichier;
  int   Taille;
  short Pos_X,Pos_Y;
  byte * buffercomp;
  byte * bufferdecomp;
  byte * ptr;
  byte pixels[320];

  filename_complet(filename,0);

  File_error=0;
  // Ouverture du fichier
  Fichier=open(filename,O_WRONLY|O_CREAT|O_TRUNC|O_BINARY,S_IREAD|S_IWRITE);
  if (Fichier!=-1)
  {
    // Allocation des buffers m�moire
    bufferdecomp=(byte *)malloc(32000);
    buffercomp  =(byte *)malloc(64066);
    // Codage de la r�solution
    buffercomp[0]=0x80;
    buffercomp[1]=0x00;
    // Codage de la palette
    PI1_code_palette((byte *)Main_palette,buffercomp+2);
    // Codage de l'image
    ptr=bufferdecomp;
    for (Pos_Y=0;Pos_Y<200;Pos_Y++)
    {
      // Codage de la ligne
      memset(pixels,0,320);
      if (Pos_Y<Main_image_height)
      {
        for (Pos_X=0;(Pos_X<320) && (Pos_X<Main_image_width);Pos_X++)
          pixels[Pos_X]=Read_pixel_old(Pos_X,Pos_Y);
      }

      // Encodage de la scanline
      PC1_1line_to_4bp(pixels,ptr,ptr+40,ptr+80,ptr+120);
      ptr+=160;
    }

    // Compression du buffer
    PC1_compress_packbits(bufferdecomp,buffercomp+34,32000,&Taille);
    Taille+=34;
    for (Pos_X=0;Pos_X<16;Pos_X++)
      buffercomp[Taille++]=0;

    if (write(Fichier,buffercomp,Taille)==Taille)
    {
      close(Fichier);
    }
    else // Error d'�criture (disque plein ou prot�g�)
    {
      close(Fichier);
      remove(filename);
      File_error=1;
    }
    // Lib�ration des buffers m�moire
    free(bufferdecomp);
    free(buffercomp);
  }
  else
  {
    close(Fichier);
    remove(filename);
    File_error=1;
  }
}
