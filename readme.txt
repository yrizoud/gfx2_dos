GrafX2 - The Ultimate 256-color bitmap paint program
Copyright (C)1996-2001 Sunset Design (G.Dorme & K.Maritaud)
-------------------------------------

Here you can find the source code of GrafX2.
You can use and modify it as you want (in the
limits fixed the GNU GPL).
But you are strongly encouraged to submit new
versions to the authors (see addresses below)
if you plan to release them.

The GFX2_SRC directory contains the current state
of our own directory when we were programming GrafX2.
The source code is mainly in French but you'll find
more explanations (in both English and French) on
our web site (see address below).

The subdirectory "DAT" contains the source code
of the program used to generate the GFX2.DAT file.

The subdirectory "CFG" contains the source code
of the GFXCFG program used for redefining keys
and recreating corrupt configuration files.

The current version of GrafX2 must be compiled
with Watcom C (we used version 10.6).
The auxiliary programs (GFXCFG, MAKEDAT, etc...)
must be compiled with Borland Turbo Pascal 7.
Both Watcom C and Borland Turbo Pascal 7 are now
FREE to use.


------- GNU G.P.L. disclaimer -------
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
-------------------------------------


You can contact the authors here:

Guillaume DORME
address: 15, rue de l'Observatoire, 87000 LIMOGES (FRANCE)
e-mail: dorme@msi.unilim.fr

Karl Maritaud
address: 10, rue de la Brasserie, 87000 LIMOGES (FRANCE)
e-mail: maritaud@ensil.unilim.fr

http://w3.ensil.unilim.fr/~maritaud/sunset
